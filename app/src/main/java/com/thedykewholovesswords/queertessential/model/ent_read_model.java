package com.thedykewholovesswords.queertessential.model;

public class ent_read_model {
    private String name;
    private String type;
    private String genre;
    private  int year;

    public ent_read_model() {
    }

    public ent_read_model(String name, String type, String genre, int year) {
        this.name = name;
        this.type = type;
        this.genre = genre;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}


