package com.thedykewholovesswords.queertessential.model;

public class shelter_model {


    private String name;
    private String phone_no;
    private String address;
    private String state;
    private boolean isVerified=false;


    public shelter_model()
    {
    }

    public shelter_model(String name, String phone_no, String address, String state)
    {
        this.name = name;
        this.phone_no = phone_no;
        this.address = address;
        this.state = state;
    }

    public shelter_model(String name, String phone_no, String address, String state, boolean isVerified) {
        this.name = name;
        this.phone_no = phone_no;
        this.address = address;
        this.state = state;
        this.isVerified = isVerified;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
