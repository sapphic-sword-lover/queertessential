package com.thedykewholovesswords.queertessential.model;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.ServerTimestamp;

public class comments_model {
    String comment;
    String UID;
    String Name;
    @ServerTimestamp
    private com.google.firebase.Timestamp timestamp;

    public comments_model(String comment, String UID, String name, com.google.firebase.Timestamp timestamp) {
        this.comment = comment;
        this.UID = UID;
        Name = name;
        this.timestamp = timestamp;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public com.google.firebase.Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(com.google.firebase.Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public comments_model() {
    }
}
