package com.thedykewholovesswords.queertessential.model;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.ServerTimestamp;

public class memes_model {

    private String UID;
    private String ImageURL;
    private  String Name;
    private String Caption;
    @ServerTimestamp
    private com.google.firebase.Timestamp timestamp;

    public memes_model() {
    }

    public memes_model(String UID, String imageURL, String name, String caption, Timestamp timestamp) {
        this.UID = UID;
        ImageURL = imageURL;
        Name = name;
        Caption = caption;
        this.timestamp = timestamp;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String caption) {
        Caption = caption;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
