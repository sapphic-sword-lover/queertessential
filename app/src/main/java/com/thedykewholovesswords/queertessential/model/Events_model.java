package com.thedykewholovesswords.queertessential.model;

import com.google.firebase.Timestamp;

public class Events_model {

    String EventName;
    String EventVenue;
    String EventCity;
    String EvenState;
    String UID;
    String EventPosterURL;
    Timestamp EventTimeStamp;

    public Events_model() {
    }

    public Events_model(String eventName, String eventVenue, String eventCity, String evenState, String eventPosterURL,String UID, Timestamp eventTimeStamp) {
        EventName = eventName;
        EventVenue = eventVenue;
        EventCity = eventCity;
        EvenState = evenState;
        EventPosterURL = eventPosterURL;
        this.UID=UID;
        EventTimeStamp = eventTimeStamp;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public String getEventVenue() {
        return EventVenue;
    }

    public void setEventVenue(String eventVenue) {
        EventVenue = eventVenue;
    }

    public String getEventCity() {
        return EventCity;
    }

    public void setEventCity(String eventCity) {
        EventCity = eventCity;
    }

    public String getEvenState() {
        return EvenState;
    }

    public void setEvenState(String evenState) {
        EvenState = evenState;
    }

    public String getEventPosterURL() {
        return EventPosterURL;
    }

    public void setEventPosterURL(String eventPosterURL) {
        EventPosterURL = eventPosterURL;
    }

    public Timestamp getEventTimeStamp() {
        return EventTimeStamp;
    }

    public void setEventTimeStamp(Timestamp eventTimeStamp) {
        EventTimeStamp = eventTimeStamp;
    }
}
