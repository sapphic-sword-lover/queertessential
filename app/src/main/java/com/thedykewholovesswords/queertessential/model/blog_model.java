package com.thedykewholovesswords.queertessential.model;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.ServerTimestamp;

public class blog_model {

    private String UID;
    private String author;
    private  String title;
    private String imageURL;
    private String content;
    @ServerTimestamp
    private com.google.firebase.Timestamp timestamp;

    public blog_model() {
    }


    public blog_model(String UID, String author, String title, String imageURL, String content, Timestamp timestamp) {
        this.UID = UID;
        this.author = author;
        this.title = title;
        this.imageURL = imageURL;
        this.content = content;
        this.timestamp = timestamp;
    }


    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
