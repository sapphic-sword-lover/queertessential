package com.thedykewholovesswords.queertessential.model;

import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;
import com.thedykewholovesswords.queertessential.doctors;

import java.sql.Date;

@IgnoreExtraProperties

public class doctors_model {

    private String name;
    private String phone_no;
    private String speciality;
    private String clinic;
    private String address;
    private String state;
    private boolean isVerified=false;



    public doctors_model()
    {

    }





    public doctors_model(String name, String phone_no, String speciality, String clinic, String address, String state, Boolean isVerified)
    {
          this.name=name;
          this.phone_no=phone_no;
          this.speciality=speciality;
          this.clinic=clinic;
          this.address=address;
          this.state=state;
          this.isVerified=isVerified;


    }




    public String getName() {
        return name;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getSpeciality() {
        return speciality;
    }

    public String getClinic() {
        return clinic;
    }

    public String getAddress() {
        return address;
    }

    public String getState() {
        return state;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }
}
