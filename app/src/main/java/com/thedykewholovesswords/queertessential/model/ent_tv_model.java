package com.thedykewholovesswords.queertessential.model;

public class ent_tv_model {

    private String name;
    private String genre;
    private int year;

    public ent_tv_model() {
    }

    public ent_tv_model(String name, String genre, int year) {
        this.name = name;
        this.genre = genre;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
