package com.thedykewholovesswords.queertessential.model;

public class psychiatrist_model {

    private String name;
    private String phone_no;
    private String clinic;
    private String address;
    private String state;
    private boolean isVerified=false;




    public psychiatrist_model()
    {

    }

    public psychiatrist_model(String name, String phone_no, String clinic, String address, String state, boolean isVerified) {
        this.name = name;
        this.phone_no = phone_no;
        this.clinic = clinic;
        this.address = address;
        this.state = state;
        this.isVerified = isVerified;
    }

    public psychiatrist_model(String name, String phone_no, String clinic, String address, String state)
    {
        this.name=name;
        this.phone_no=phone_no;
        this.clinic=clinic;
        this.address=address;
        this.state=state;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}



