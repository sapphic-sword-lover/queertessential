package com.thedykewholovesswords.queertessential.model;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.type.Date;

@IgnoreExtraProperties

public class chat_message_model {

    private String UID;
    private  String name;
    private String message;
    @ServerTimestamp
    private com.google.firebase.Timestamp timestamp;


    public chat_message_model(String UID, String name, String message, com.google.firebase.Timestamp timestamp) {
        this.UID = UID;
        this.name = name;
        this.message = message;
        this.timestamp = timestamp;
    }

    public chat_message_model() {

    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public com.google.firebase.Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(com.google.firebase.Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
