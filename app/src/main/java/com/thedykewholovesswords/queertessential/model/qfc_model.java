package com.thedykewholovesswords.queertessential.model;

public class qfc_model {

    private String name;
    private String address;
    private String state;
    private boolean isVerified=false;

    public qfc_model() {
    }

    public qfc_model(String name, String address, String state) {
        this.name = name;
        this.address = address;
        this.state = state;
    }

    public qfc_model(String name, String address, String state, boolean isVerified) {
        this.name = name;
        this.address = address;
        this.state = state;
        this.isVerified = isVerified;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
