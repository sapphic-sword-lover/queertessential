package com.thedykewholovesswords.queertessential.model;

public class sph_model {

    private String name;
    private String phone_no;
    private boolean isVerified=false;

    public sph_model() {
    }

    public sph_model(String name, String phone_no) {
        this.name = name;
        this.phone_no = phone_no;
    }

    public sph_model(String name, String phone_no, boolean isVerified) {
        this.name = name;
        this.phone_no = phone_no;
        this.isVerified = isVerified;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }
}
