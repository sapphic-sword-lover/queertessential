package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.model.ss_model;

public class safeSpaces extends AppCompatActivity {
    private RecyclerView mSs_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_spaces);
        getSupportActionBar().setTitle("Safe Spaces");

        mSs_list=findViewById(R.id.ss_list_rv);

        fStore=FirebaseFirestore.getInstance();

        //Query
        Query query= fStore.collection("safe_spaces").orderBy("state");

        //Recyler handler

        FirestoreRecyclerOptions<ss_model> options= new FirestoreRecyclerOptions.Builder<ss_model>().setQuery(query,ss_model.class).build();

        adapter= new FirestoreRecyclerAdapter<ss_model, ssViewHolder>(options) {
            @NonNull
            @Override
            public ssViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.ss_list_layout,parent,false);



                return new ssViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull ssViewHolder holder, int position, @NonNull ss_model model) {
                holder.list_name.setText(model.getName());
                holder.list_address.setText(model.getAddress());
                holder.list_state.setText(model.getState());
                if(model.isVerified()==true)
                {
                    holder.list_verified.setVisibility(View.VISIBLE);
                }
            }
        };



        mSs_list.setHasFixedSize(true);
        mSs_list.setLayoutManager(new LinearLayoutManager(this));
        mSs_list.setAdapter(adapter);



    }

    public void open_add_ss(View view)
    {

        startActivity(new Intent(getApplicationContext(),ss_add.class));
    }




    private class ssViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_ph,list_address, list_state;
        ImageView list_verified;


        public ssViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.ss_lay_name);
            list_address=itemView.findViewById(R.id.ss_lay_add);
            list_state=itemView.findViewById(R.id.ss_lay_state);
            list_verified=itemView.findViewById(R.id.ss_verified_imageview);


        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_menu,menu);

        MenuItem item=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                processSearch(s);
                return false;


            }

            @Override
            public boolean onQueryTextChange(String s) {
                processSearch(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void processSearch(String s) {

        if(TextUtils.isEmpty(s))
        {
            //Query
            Query query= fStore.collection("safe_spaces").orderBy("state");

            //Recyler handler

            FirestoreRecyclerOptions<ss_model> options= new FirestoreRecyclerOptions.Builder<ss_model>().setQuery(query,ss_model.class).build();

            adapter= new FirestoreRecyclerAdapter<ss_model, ssViewHolder>(options) {
                @NonNull
                @Override
                public ssViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.ss_list_layout,parent,false);



                    return new ssViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull ssViewHolder holder, int position, @NonNull ss_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mSs_list.setHasFixedSize(true);
            mSs_list.setLayoutManager(new LinearLayoutManager(this));
            mSs_list.setAdapter(adapter);
            adapter.startListening();


        } else
        {
            //Query
            Query query= fStore.collection("safe_spaces").orderBy("state").whereEqualTo("state",s);

            //Recyler handler

            FirestoreRecyclerOptions<ss_model> options= new FirestoreRecyclerOptions.Builder<ss_model>().setQuery(query,ss_model.class).build();

            adapter= new FirestoreRecyclerAdapter<ss_model, ssViewHolder>(options) {
                @NonNull
                @Override
                public ssViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.ss_list_layout,parent,false);



                    return new ssViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull ssViewHolder holder, int position, @NonNull ss_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mSs_list.setHasFixedSize(true);
            mSs_list.setLayoutManager(new LinearLayoutManager(this));
            mSs_list.setAdapter(adapter);
            adapter.startListening();

        }

    }




}