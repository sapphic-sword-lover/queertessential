package com.thedykewholovesswords.queertessential.Ent_tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.ent_anime_model;
import com.thedykewholovesswords.queertessential.model.ent_tv_model;

public class frag_anime extends Fragment {

    View V;
    private RecyclerView mAnime_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V= inflater.inflate(R.layout.ent_anime,container,false);


        mAnime_list=V.findViewById(R.id.ent_anime_disp_rv);
        fStore=FirebaseFirestore.getInstance();

        Query query= fStore.collection("anime").orderBy("year", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<ent_anime_model> options= new FirestoreRecyclerOptions.Builder<ent_anime_model>().setQuery(query,ent_anime_model.class).build();

        adapter= new FirestoreRecyclerAdapter<ent_anime_model, animeViewHolder>(options) {
            @NonNull
            @Override
            public animeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ent_disp_three,parent,false);



                return new animeViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull animeViewHolder holder, int position, @NonNull ent_anime_model model) {
                holder.list_name.setText(model.getName());
                holder.list_genre.setText(model.getGenre()+", "+model.getYear());


            }
        };



        mAnime_list.setHasFixedSize(true);
        mAnime_list.setLayoutManager(new LinearLayoutManager(requireContext()));
        mAnime_list.setAdapter(adapter);

        return V;
    }


    private class animeViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_genre;


        public animeViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.ent_three_name);
            list_genre=itemView.findViewById(R.id.ent_three_genre);


        }
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }


}
