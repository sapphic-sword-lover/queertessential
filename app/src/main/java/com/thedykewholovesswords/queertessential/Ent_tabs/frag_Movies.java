package com.thedykewholovesswords.queertessential.Ent_tabs;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.doctors;
import com.thedykewholovesswords.queertessential.model.doctors_model;
import com.thedykewholovesswords.queertessential.model.ent_movies_model;
import com.thedykewholovesswords.queertessential.ui.main.PlaceholderFragment;

public class frag_Movies extends Fragment {

    View V;
    private RecyclerView mMovies_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        V= inflater.inflate(R.layout.ent_movies,container,false);

        mMovies_list=V.findViewById(R.id.ent_mov_disp_rv);
        fStore=FirebaseFirestore.getInstance();

        Query query= fStore.collection("movies").orderBy("year", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<ent_movies_model> options= new FirestoreRecyclerOptions.Builder<ent_movies_model>().setQuery(query,ent_movies_model.class).build();

        adapter= new FirestoreRecyclerAdapter<ent_movies_model, moviesViewHolder>(options) {
            @NonNull
            @Override
            public moviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ent_disp_three,parent,false);



                return new moviesViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull moviesViewHolder holder, int position, @NonNull ent_movies_model model) {
                holder.list_name.setText(model.getName());
                holder.list_genre.setText(model.getGenre()+", "+model.getYear());


            }
        };



        mMovies_list.setHasFixedSize(true);
        mMovies_list.setLayoutManager(new LinearLayoutManager(requireContext()));
        mMovies_list.setAdapter(adapter);

        //View Holder



        return V;


    }

    private class moviesViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_genre,list_year;


        public moviesViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.ent_three_name);
            list_genre=itemView.findViewById(R.id.ent_three_genre);


        }
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
}
