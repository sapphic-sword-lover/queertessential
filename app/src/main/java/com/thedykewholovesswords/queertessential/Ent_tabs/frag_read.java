package com.thedykewholovesswords.queertessential.Ent_tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.ent_movies_model;
import com.thedykewholovesswords.queertessential.model.ent_read_model;

public class frag_read extends Fragment {

    View V;
    private RecyclerView mReads_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V= inflater.inflate(R.layout.ent_reads,container,false);

        mReads_list=V.findViewById(R.id.ent_reads_disp_rv);
        fStore=FirebaseFirestore.getInstance();

        Query query= fStore.collection("reads").orderBy("year", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<ent_read_model> options= new FirestoreRecyclerOptions.Builder<ent_read_model>().setQuery(query,ent_read_model.class).build();

        adapter= new FirestoreRecyclerAdapter<ent_read_model, readViewHolder>(options) {
            @NonNull
            @Override
            public readViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ent_disp_three,parent,false);



                return new readViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull readViewHolder holder, int position, @NonNull ent_read_model model) {
                holder.list_name.setText(model.getName());
                holder.list_genre.setText(model.getType()+",  "+model.getGenre()+",  "+model.getYear());


            }
        };



        mReads_list.setHasFixedSize(true);
        mReads_list.setLayoutManager(new LinearLayoutManager(requireContext()));
        mReads_list.setAdapter(adapter);


        return V;

    }

    private class readViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_genre,list_year;


        public readViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.ent_three_name);
            list_genre=itemView.findViewById(R.id.ent_three_genre);


        }
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
}
