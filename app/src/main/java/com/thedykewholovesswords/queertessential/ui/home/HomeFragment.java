package com.thedykewholovesswords.queertessential.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.SliderAdapter.HomeSliderAdapter;

public class HomeFragment extends Fragment {

   SliderView sliderView;
   HomeSliderAdapter adapter;

   int[] images={R.drawable.slide1,R.drawable.slide3fixed,R.drawable.slide3};


    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        sliderView=root.findViewById(R.id.slider_view);
        adapter=new HomeSliderAdapter(images);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION);
        sliderView.startAutoCycle();






        return root;
    }
}