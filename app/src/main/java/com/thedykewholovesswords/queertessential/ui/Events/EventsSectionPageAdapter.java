package com.thedykewholovesswords.queertessential.ui.Events;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.thedykewholovesswords.queertessential.EventsTabs.EventsUpcoming;
import com.thedykewholovesswords.queertessential.EventsTabs.EventsPast;
import com.thedykewholovesswords.queertessential.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class EventsSectionPageAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.events_tab_text_1,R.string.events_tab_text2};
    private final Context mContext;

    public EventsSectionPageAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;
        switch (position) {
            case 0:
                fragment=new EventsUpcoming();
                break;

            case 1:
                fragment=new EventsPast();
                break;






        }
        return fragment;








    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }
}