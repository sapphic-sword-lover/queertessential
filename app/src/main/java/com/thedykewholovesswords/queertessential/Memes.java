package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.thedykewholovesswords.queertessential.MemesTabs.MemesNotification;
import com.thedykewholovesswords.queertessential.MemesTabs.memes_feed;

public class Memes extends AppCompatActivity {

    private BottomNavigationView Memes_BNV;
    private memes_feed memesFeed;
    private MemesNotification memesNotification;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memes);
        getSupportActionBar().setTitle("Memes");

        Memes_BNV=findViewById(R.id.memes_bottom_nav);

        memesFeed=new memes_feed();
        memesNotification=new MemesNotification();
        FragmentSetter(memesFeed);


        Memes_BNV.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.memes_nav_feed:
                        FragmentSetter(memesFeed);
                        return true;
                    case R.id.memes_nav_new:
                        startActivity(new Intent(getApplicationContext(),MemesAdd.class));
                        return true;
                    case R.id.memes_nav_notifications:
                        FragmentSetter(memesNotification);
                }
                return false;
            }
        });






    }


    public void FragmentSetter(Fragment fragment)
    {
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.memes_frame_layout,fragment);
        fragmentTransaction.commit();
    }

}