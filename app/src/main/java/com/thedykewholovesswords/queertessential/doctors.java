package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.Query;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.thedykewholovesswords.queertessential.model.doctors_model;

public class doctors extends AppCompatActivity {
   private RecyclerView mDoctors_list;
   private FirebaseFirestore fStore;
   private  FirestoreRecyclerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors);
        getSupportActionBar().setTitle("Doctors");
        mDoctors_list=findViewById(R.id.doctors_list_rv);



        fStore=FirebaseFirestore.getInstance();

        //Query
        Query query= fStore.collection("doctors").orderBy("state");
        //Recyler handler

        FirestoreRecyclerOptions<doctors_model>options= new FirestoreRecyclerOptions.Builder<doctors_model>().setQuery(query,doctors_model.class).build();

        adapter= new FirestoreRecyclerAdapter<doctors_model, doctorsViewHolder>(options) {
            @NonNull
            @Override
            public doctorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_layout,parent,false);

                return new doctorsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull doctorsViewHolder holder, int position, @NonNull doctors_model model) {


                       holder.list_name.setText(model.getName());
                       holder.list_ph.setText(model.getPhone_no());
                       holder.list_speciality.setText(model.getSpeciality());
                       holder.list_clinic.setText(model.getClinic());
                       holder.list_address.setText(model.getAddress());
                       holder.list_state.setText(model.getState());
                       if(model.isVerified()==true)
                       {
                           holder.list_verified.setVisibility(View.VISIBLE);
                       }


            }
        };



     mDoctors_list.setHasFixedSize(true);
     mDoctors_list.setLayoutManager(new LinearLayoutManager(this));
     mDoctors_list.setAdapter(adapter);

        //View Holder

    }

    public void open_addfragement(View view) {


        startActivity(new Intent(getApplicationContext(),doctor_add.class));


    }

    private class doctorsViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_ph,list_speciality, list_clinic,list_address, list_state;
        ImageView list_verified;


        public doctorsViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.doc_lay_name);
            list_ph=itemView.findViewById(R.id.doc_lay_ph);
            list_speciality=itemView.findViewById(R.id.doc_lay_spec);
            list_clinic=itemView.findViewById(R.id.doc_lay_clinic);
            list_address=itemView.findViewById(R.id.doc_lay_add);
            list_state=itemView.findViewById(R.id.doc_lay_state);
            list_verified=itemView.findViewById(R.id.doc_verified_imageview);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_menu,menu);

        MenuItem item=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                processSearch(s);
                return false;


            }

            @Override
            public boolean onQueryTextChange(String s) {
                processSearch(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void processSearch(String s) {

        if(TextUtils.isEmpty(s)){


            //Query
            Query query= fStore.collection("doctors").orderBy("state");
            //Recyler handler

            FirestoreRecyclerOptions<doctors_model>options= new FirestoreRecyclerOptions.Builder<doctors_model>().setQuery(query,doctors_model.class).build();

            adapter= new FirestoreRecyclerAdapter<doctors_model, doctorsViewHolder>(options) {
                @NonNull
                @Override
                public doctorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_layout,parent,false);



                    return new doctorsViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull doctorsViewHolder holder, int position, @NonNull doctors_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_ph.setText(model.getPhone_no());
                    holder.list_speciality.setText(model.getSpeciality());
                    holder.list_clinic.setText(model.getClinic());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mDoctors_list.setHasFixedSize(true);
            mDoctors_list.setLayoutManager(new LinearLayoutManager(this));
            mDoctors_list.setAdapter(adapter);

            adapter.startListening();






        } else {
        //Query
        Query query= fStore.collection("doctors").orderBy("state").whereEqualTo("state",s);
        //Recyler handler

        FirestoreRecyclerOptions<doctors_model>options= new FirestoreRecyclerOptions.Builder<doctors_model>().setQuery(query,doctors_model.class).build();

        adapter= new FirestoreRecyclerAdapter<doctors_model, doctorsViewHolder>(options) {
            @NonNull
            @Override
            public doctorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_layout,parent,false);



                return new doctorsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull doctorsViewHolder holder, int position, @NonNull doctors_model model) {
                holder.list_name.setText(model.getName());
                holder.list_ph.setText(model.getPhone_no());
                holder.list_speciality.setText(model.getSpeciality());
                holder.list_clinic.setText(model.getClinic());
                holder.list_address.setText(model.getAddress());
                holder.list_state.setText(model.getState());
                if(model.isVerified()==true)
                {
                    holder.list_verified.setVisibility(View.VISIBLE);
                }
            }
        };



        mDoctors_list.setHasFixedSize(true);
        mDoctors_list.setLayoutManager(new LinearLayoutManager(this));
        mDoctors_list.setAdapter(adapter);

        adapter.startListening();
        }





    }
}