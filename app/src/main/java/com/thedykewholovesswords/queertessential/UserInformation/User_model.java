package com.thedykewholovesswords.queertessential.UserInformation;

public class User_model {

    String dName;
    String email;

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User_model() {
    }

    public User_model(String dName, String email) {
        this.dName = dName;
        this.email = email;
    }
}
