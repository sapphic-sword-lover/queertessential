package com.thedykewholovesswords.queertessential.UserInformation;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.thedykewholovesswords.queertessential.ChatRoom;
import com.thedykewholovesswords.queertessential.R;

import static android.os.Handler.*;

public class UserRetriver {

    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String UserID;
    public static String DName;
    User_model um=new User_model();

    public UserRetriver(String UserID) {
        fAuth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();
        this.UserID=UserID;

    }



   /* public String getDisplayName()
    {   Log.d("UR123","Function was run");
        readData(new FirebaseCallBack() {
            @Override
            public void onCallback(User_model userModel) {
                DName=userModel.getdName();
                Log.d("UR123","From Read Data " +DName);
                return;

            }
        });
        Log.d("UR123","From getDisplayName"+DName);
        return  DName;
    }*/

        public interface FirebaseCallBack{
        void onCallback(User_model userModel);

    }
    public void readData(FirebaseCallBack firebaseCallBack)
    {
        DocumentReference Dr=fStore.collection("users").document(UserID);
        Dr.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                um=documentSnapshot.toObject(User_model.class);
                firebaseCallBack.onCallback(um);


            }
        });

    }
}
