package com.thedykewholovesswords.queertessential.Swipe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.BlogFullView;
import com.thedykewholovesswords.queertessential.MyPostFullView;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.BlogRvAdapter;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.MyPostRvAdapter;
import com.thedykewholovesswords.queertessential.model.blog_model;

public class MyPosts extends AppCompatActivity {

    private FirebaseFirestore fstore;
    private MyPostRvAdapter adapter;
    private FirebaseAuth fAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);
        getSupportActionBar().setTitle("My Posts");
        fstore= FirebaseFirestore.getInstance();
        fAuth=FirebaseAuth.getInstance();

        setUpRecyclerView();
    }

    private void setUpRecyclerView() {

        String uid=fAuth.getCurrentUser().getUid();

        Query query=fstore.collection("blogs").orderBy("timestamp", Query.Direction.DESCENDING).whereEqualTo("uid",uid);
        FirestoreRecyclerOptions<blog_model> options=new FirestoreRecyclerOptions.Builder<blog_model>().setQuery(query,blog_model.class).build();
        adapter=new MyPostRvAdapter(options);
        RecyclerView recyclerView=findViewById(R.id.myposts_rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new MyPostRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {

                blog_model bmm=documentSnapshot.toObject(blog_model.class);
                String id=documentSnapshot.getId();



                Intent intent=new Intent(getApplicationContext(), MyPostFullView.class);
                Bundle bundle=new Bundle();
                bundle.putString("ID",id);
                intent.putExtras(bundle);

                startActivity(intent);

            }
        });





    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}