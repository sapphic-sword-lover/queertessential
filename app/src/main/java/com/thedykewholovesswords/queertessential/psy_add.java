package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.thedykewholovesswords.queertessential.model.psychiatrist_model;

public class psy_add extends AppCompatActivity {

    EditText mName,mClinic,mPh,mAddress,mState;
    Button mSubmit;
    FirebaseFirestore fStore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psy_add);
        getSupportActionBar().setTitle("Add To List of Psychiatrists");

        mName=findViewById(R.id.psychiatrist_add_name);
        mPh=findViewById(R.id.psychiatrist_add_ph);
        mClinic=findViewById(R.id.psychiatrist_add_clinic);
        mAddress=findViewById(R.id.psychiatrist_add_add);
        mState=findViewById(R.id.psychiatrist_add_state);

        mSubmit=findViewById(R.id.psychiatrist_add_submit);

        fStore=FirebaseFirestore.getInstance();

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name=mName.getText().toString().trim();
                String ph=mPh.getText().toString().trim();
                String Clinic=mClinic.getText().toString().trim();
                String Add=mAddress.getText().toString().trim();
                String State=mState.getText().toString().trim();
                onBackPressed();





                if(TextUtils.isEmpty(Name))
                {
                    mName.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(ph))
                {
                    mPh.setError("Field Mandatory");
                    return;
                }
                else if(ph.length()<9 || ph.length()>11)
                {
                    mPh.setError("Phone Number must be 10 digit or 11 Digit");
                    return;
                }



                if(TextUtils.isEmpty(Clinic))
                {
                    mClinic.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(Add))
                {
                    mAddress.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(State))
                {
                    mState.setError("Field Mandatory");
                    return;
                }


                psychiatrist_model dm=new psychiatrist_model(Name,ph,Clinic,Add,State);

                DocumentReference documentReference=fStore.collection("psychiatrists").document();

                documentReference.set(dm).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "Entry Added Successfully", Toast.LENGTH_SHORT).show();



                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("add123",""+e);
                    }
                });



            }
        });

    }
}