package com.thedykewholovesswords.queertessential;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.thedykewholovesswords.queertessential.ui.main.SectionsPagerAdapter;

public class entertainment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // getSupportActionBar().setTitle("Entertainment");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainment);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

    }

    public void open_add_ent(View view) {

        startActivity(new Intent(getApplicationContext(),ent_add.class));

    }
}