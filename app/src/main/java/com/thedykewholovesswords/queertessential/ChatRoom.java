package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.ChatRvAdapter;
import com.thedykewholovesswords.queertessential.model.chat_message_model;

public class ChatRoom extends AppCompatActivity {
    private FirebaseFirestore fstore;
    private FirebaseAuth fAuth;
    String UID;
    private CollectionReference chatRef;
    private ChatRvAdapter adapter;
    static String DISPLAY_NAME;
    ImageButton button;
    EditText mMessage;
    TextView mDN;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        getSupportActionBar().setTitle("Queertesential Chatroom");
        button=findViewById(R.id.cr_mess_send_btn);
        mMessage=findViewById(R.id.cr_mess_ET);
        fstore=FirebaseFirestore.getInstance();
        fAuth=FirebaseAuth.getInstance();

        mDN=findViewById(R.id.cr_display_name);

        UID=fAuth.getCurrentUser().getUid().toString();
        DocumentReference documentReference=fstore.collection("users").document(UID);

        documentReference.addSnapshotListener( new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {

                if (error != null) {
                    Log.w("abc123", "Listen failed.", error);
                    return;
                }

                if (value != null && value.exists()) {
                    //String DN=value.getString("dName");
                    // String Email=value.getString("email");
                    mDN.setText(value.getString("dName"));
                    // Log.d("abc123", "Current data: "+DN);
                    //um=new User_model(DN,Email);





                } else {
                    Log.d("abc123", "Current data: null");
                }





            }
        });



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String UUID=fAuth.getCurrentUser().getUid();
                String DN=mDN.getText().toString();
                String message=mMessage.getText().toString().trim();
                mMessage.setText("");

                if(TextUtils.isEmpty(message))
                {
                    return;
                }


                chat_message_model cmm=new chat_message_model();
                cmm.setUID(UUID);
                cmm.setName(DN);
                cmm.setMessage(message);


                DocumentReference documentReference=fstore.collection("chatroom").document();
                documentReference.set(cmm).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mMessage.setText("");
                        setUpRecyclerView();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ChatRoom.this, "Error! "+e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });





            }
        });


        setUpRecyclerView();


    }



    private void setUpRecyclerView() {
        Query query=fstore.collection("chatroom").orderBy("timestamp");
        FirestoreRecyclerOptions<chat_message_model> options=new FirestoreRecyclerOptions.Builder<chat_message_model>().setQuery(query,chat_message_model.class).build();
        adapter= new ChatRvAdapter(options);
        RecyclerView recyclerView=findViewById(R.id.chat_room_rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm=new LinearLayoutManager(this);
        lm.setStackFromEnd(true);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();


        /*adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            public void onItemRangeInserted(int positionStart, int itemCount) {
                recyclerView.smoothScrollToPosition(adapter.getItemCount());
            }
        });

         */

        //recyclerView.smoothScrollToPosition(adapter.getItemCount());
       // int count=adapter.getItemCount();
        //Toast.makeText(this, ""+count, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}