package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.model.Events_model;
import com.thedykewholovesswords.queertessential.model.blog_model;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;

public class EventsAdd extends AppCompatActivity {

    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    FirebaseStorage fCloud;



    Calendar dateobj=Calendar.getInstance();

    EditText mEname, mEvenue,mEcity,mEstate;
    TextView mEdate,mEtime;
    ImageView mEPoster;
    Button mSubmit;
    Timestamp ts;
    Dialog dialog;
    LottieAnimationView load;


    StorageReference SR;
    DocumentReference DR;

    private  static final int PICK_IMAGE_REQUEST=1;
    private Uri mImageURI;
    static String dlURL;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_add);
        getSupportActionBar().setTitle("Add Events");

        mEname=findViewById(R.id.events_add_name);
        mEdate=findViewById(R.id.events_add_date);
        mEtime=findViewById(R.id.events_add_time);
        mEvenue=findViewById(R.id.events_add_venue);
        mEcity=findViewById(R.id.events_add_city);
        mEstate=findViewById(R.id.events_add_state);
        mEPoster=findViewById(R.id.events_add_image);

        mSubmit=findViewById(R.id.events_add_submit_btn);

        fAuth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();
        fCloud=FirebaseStorage.getInstance();

        dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);

        SR=fCloud.getReference("Events_Posters/Posters");
        DR=fStore.collection("events").document();


        mEdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateSelector();
            }
        });
        mEtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimeSelector();
            }
        });

        mEPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilePicker();

            }
        });

      // Date d1=dateobj.getTime();
       //ts=new Timestamp(d1);

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UploadData();

            }
        });







    }

    private void UploadData() {

        String Name=mEname.getText().toString();
        String Venue=mEvenue.getText().toString();
        String City=mEcity.getText().toString();
        String State=mEstate.getText().toString();

        String Date=mEdate.getText().toString();
        String Time=mEtime.getText().toString();

        if(mImageURI==null)
        {
            Toast.makeText(this, "Error! Select an Image", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(Name))
        {
            mEname.setError("Required Field");
            return;
        }
        if(TextUtils.isEmpty(Venue))
        {
            mEvenue.setError("Required Field");
            return;
        }
        if(TextUtils.isEmpty(City))
        {
            mEcity.setError("Required Field");
            return;
        }
        if(TextUtils.isEmpty(State))
        {
            mEstate.setError("Required Field");
            return;
        }
        if (TextUtils.isEmpty(Date)||TextUtils.isEmpty(Time))
        {
            Toast.makeText(this, "Error! "+ "Please Choose Date and Time", Toast.LENGTH_SHORT).show();
            return;
        }
        try{
            Date d1=dateobj.getTime();
            ts=new Timestamp(d1);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        StorageReference fileReference=SR.child(System.currentTimeMillis()+"."+getFileExtension(mImageURI));
        fileReference.putFile(mImageURI)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                        fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Uri dlUri=uri;
                                dlURL=dlUri.toString();

                                Events_model Em=new Events_model();
                                Em.setEventName(Name);
                                Em.setEventTimeStamp(ts);
                                Em.setEventVenue(Venue);
                                Em.setEventCity(City);
                                Em.setEvenState(State);
                                Em.setEventPosterURL(dlURL);
                                Em.setUID(fAuth.getCurrentUser().getUid().toString());

                                DR.set(Em).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {


                                        dialog.dismiss();
                                        mEPoster.setImageDrawable(getDrawable(R.drawable.ic_baseline_add_photo_alternate_24));
                                        mEname.setText("");
                                        mEdate.setText("");
                                        mEtime.setText("");
                                        mEvenue.setText("");
                                        mEcity.setText("");
                                        mEstate.setText("");
                                        Toast.makeText(EventsAdd.this, "Upload Successful", Toast.LENGTH_SHORT).show();
                                        onBackPressed();


                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(EventsAdd.this, "Error! "+e.getMessage(), Toast.LENGTH_LONG).show();

                                    }
                                });


                            }
                        });





                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(EventsAdd.this, "Error! "+e.getMessage(), Toast.LENGTH_LONG).show();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {

                ProgressBar mProgress=dialog.findViewById(R.id.upload_progress);
                load = dialog.findViewById(R.id.loading_progress);
                load.setVisibility(View.VISIBLE);
                dialog.show();


            }
        });
    }


    private String getFileExtension(Uri uri) {
        ContentResolver CR=getContentResolver();
        MimeTypeMap mime=MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(CR.getType(uri));
    }
    private void openFilePicker() {
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICK_IMAGE_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {  try {
            mImageURI=data.getData();
            Picasso.get().load(mImageURI).into(mEPoster);

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        }
    }

    private void openTimeSelector() {
        Calendar calendar=Calendar.getInstance();
        int HOUR=calendar.get(Calendar.HOUR_OF_DAY);
        int MIN=calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog=new TimePickerDialog(this, R.style.TimestampPicker,new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String TPtime=hourOfDay+":"+minute;
                mEtime.setText(TPtime);
                dateobj.set(Calendar.HOUR,hourOfDay);
                dateobj.set(Calendar.MINUTE,minute);

            }
        },HOUR,MIN,true);
        timePickerDialog.show();


    }

    private void openDateSelector() {
        Calendar calendar=Calendar.getInstance();

        int Year=calendar.get(Calendar.YEAR);
        int Month=calendar.get(Calendar.MONTH);
        int day=calendar.get(Calendar.DATE);
        DatePickerDialog datePickerDialog=new DatePickerDialog(this,R.style.TimestampPicker,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String DPdate=dayOfMonth+"/"+month+"/"+dayOfMonth;
                mEdate.setText(DPdate);
                dateobj.set(Calendar.DATE,dayOfMonth);
                dateobj.set(Calendar.MONTH,month);
                dateobj.set(Calendar.YEAR,year);


            }
        },Year,Month,day);



        datePickerDialog.show();


    }
}