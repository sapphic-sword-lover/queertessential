package com.thedykewholovesswords.queertessential.RecycleViewAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.memes_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MemesRvAdapter extends FirestoreRecyclerAdapter<memes_model,MemesRvAdapter.MemesHolder> {

    public OnItemClickListener listener;


    FirebaseFirestore fStore=FirebaseFirestore.getInstance();
    FirebaseAuth fAuth=FirebaseAuth.getInstance();

    public MemesRvAdapter(@NonNull FirestoreRecyclerOptions<memes_model>options)
    {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull MemesHolder holder, int position, @NonNull memes_model model) {
        holder.setIsRecyclable(false);

        try
        {
            Date ts = model.getTimestamp().toDate();

            DateFormat df = new SimpleDateFormat("MMM dd, yyyy  HH:mm");
            String tss = df.format(ts);

            holder.mName.setText(model.getName());
            holder.mTimeStamp.setText(tss);
            holder.mCaption.setText(model.getCaption());
            Picasso.get().load(model.getImageURL()).into(holder.mImage);

            DocumentSnapshot DS=getSnapshots().getSnapshot(holder.getAdapterPosition());
            String PostID=DS.getId();

            //Get Comment Counts
            fStore.collection("memes").document(PostID).collection("comments").addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(!value.isEmpty())
                    {
                        int count=value.size();
                        holder.mCommentCount.setText(""+count+" Comments");

                    }
                    else
                    {
                        holder.mCommentCount.setText("0 Comments");
                    }

                }
            });


            //Get Like Counts
            fStore.collection("memes").document(PostID).collection("likes").addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(!value.isEmpty())
                    {
                        int count=value.size();
                        holder.mLikeCount.setText(""+count+" Likes");

                    }
                    else
                    {
                        holder.mLikeCount.setText("0 Likes");
                    }

                }
            });
            //GetLikes
            fStore.collection("memes").document(PostID).collection("likes").document(fAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                    if (value.exists()) {
                        holder.Like_btn.setImageResource(R.drawable.ic_baseline_like_red);

                    } else
                    {
                        holder.Like_btn.setImageResource(R.drawable.ic_baseline_like_gray);
                    }

                }
            });



        }catch (Exception e)
        {
            e.printStackTrace();
        }



    }


    @NonNull
    @Override
    public MemesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_memes_short_display,parent,false);
        return new MemesHolder(view);
    }


    public class MemesHolder extends RecyclerView.ViewHolder
    {
        TextView mName,mTimeStamp,mCaption;
        TextView mLikeCount,mCommentCount;
        ImageView mImage;
        ImageButton Like_btn,Comment_btn;
        int Pos;

        public MemesHolder(@NonNull View itemView) {
            super(itemView);
            Pos=getAdapterPosition();
            mName=itemView.findViewById(R.id.memes_full_name);
            mTimeStamp=itemView.findViewById(R.id.memes_full_timestamp);
            mCaption=itemView.findViewById(R.id.memes_full_caption);
            mImage=itemView.findViewById(R.id.memes_short_image);
            Like_btn=itemView.findViewById(R.id.memes_short_like_btn);
            mLikeCount=itemView.findViewById(R.id.memes_short_likes_count);
            Comment_btn=itemView.findViewById(R.id.memes_short_comment_btn);
            mCommentCount=itemView.findViewById(R.id.memes_short_comment_count);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION && listener!=null)
                    {
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

            Like_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position=getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION && listener!=null)
                    {
                       listener.onLikeClick(getSnapshots().getSnapshot(position),position,Like_btn,mLikeCount);

                    }

                }
            });
            Comment_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION && listener!=null)
                    {
                        listener.onCommentClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

        }
    }
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
        void onLikeClick(DocumentSnapshot documentSnapshot,int position,ImageButton Like, TextView LikeCount);
        void onCommentClick(DocumentSnapshot documentSnapshot, int position);
    }
    public void setOnItemClickListener(MemesRvAdapter.OnItemClickListener listener){
        this.listener=listener;

    }






}
