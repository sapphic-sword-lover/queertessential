package com.thedykewholovesswords.queertessential.RecycleViewAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.comments_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommentsRvAdapter extends FirestoreRecyclerAdapter<comments_model, CommentsRvAdapter.CommentsHolder> {

    public CommentsRvAdapter(@NonNull FirestoreRecyclerOptions<comments_model> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull CommentsRvAdapter.CommentsHolder holder, int position, @NonNull comments_model model) {

        try{
            Date ts = model.getTimestamp().toDate();

            DateFormat df = new SimpleDateFormat("MMM dd, yyyy  HH:mm");
            String tss = df.format(ts);

            holder.mName.setText(model.getName());
            holder.mComment.setText(model.getComment());
            holder.mTimestamp.setText(tss);

        }catch (Exception e)
        {
            e.printStackTrace();

        }

    }

    @NonNull
    @Override
    public CommentsRvAdapter.CommentsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_memes_comments,parent,false);
        return new CommentsHolder(view);
    }

    public class CommentsHolder extends RecyclerView.ViewHolder {

        TextView mName,mComment,mTimestamp;

        public CommentsHolder(@NonNull View itemView) {
            super(itemView);
            mName=itemView.findViewById(R.id.memes_comment_username);
            mComment=itemView.findViewById(R.id.memes_comment_comment);
            mTimestamp=itemView.findViewById(R.id.memes_comment_time);
        }
    }
}
