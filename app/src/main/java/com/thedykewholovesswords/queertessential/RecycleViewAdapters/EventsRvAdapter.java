package com.thedykewholovesswords.queertessential.RecycleViewAdapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.Events_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventsRvAdapter extends FirestoreRecyclerAdapter<Events_model,EventsRvAdapter.EventsHolder> {
    public EventsRvAdapter.OnItemClickListener listener;

    public EventsRvAdapter(@NonNull FirestoreRecyclerOptions<Events_model> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull EventsHolder holder, int position, @NonNull Events_model model) {


        try{
            Date ts = model.getEventTimeStamp().toDate();

            DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm");
            String tss = df.format(ts);


            holder.mName.setText(model.getEventName());
            holder.mTimestamp.setText(tss);
            holder.mVenue.setText(model.getEventVenue());
            holder.mLocation.setText(model.getEventCity()+", "+model.getEvenState());
            Picasso.get().load(model.getEventPosterURL()).into(holder.mImageview);
            Log.d("Events123","Test "+model.getEventName()+" "+ tss+" "+model.getEventCity());



        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @NonNull
    @Override
    public EventsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_events_short_display,parent,false);
        return new EventsHolder(view);

    }

    public class EventsHolder extends RecyclerView.ViewHolder {
        TextView mName,mTimestamp, mVenue,mLocation;
        ImageView mImageview;




        public EventsHolder(@NonNull View itemView) {
            super(itemView);
            mName=itemView.findViewById(R.id.events_short_name);
            mTimestamp=itemView.findViewById(R.id.events_short_timestamp);
            mVenue=itemView.findViewById(R.id.events_short_venue);
            mLocation=itemView.findViewById(R.id.events_short_city_state);
            mImageview=itemView.findViewById(R.id.events_short_poster);
            mImageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION && listener!=null)
                    {
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });




        }
    }
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);

    }
    public void setOnItemClickListener(EventsRvAdapter.OnItemClickListener listener){
        this.listener=listener;

    }
}
