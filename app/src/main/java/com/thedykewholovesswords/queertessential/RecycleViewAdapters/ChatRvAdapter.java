package com.thedykewholovesswords.queertessential.RecycleViewAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.chat_message_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatRvAdapter extends FirestoreRecyclerAdapter <chat_message_model, ChatRvAdapter.ChatHolder>{

    public ChatRvAdapter(@NonNull FirestoreRecyclerOptions<chat_message_model> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull chat_message_model model) {
        holder.setIsRecyclable(false);

       try {
           Date ts = model.getTimestamp().toDate();

           DateFormat df = new SimpleDateFormat("MMM dd, yyyy  HH:mm");
           String tss = df.format(ts);


           holder.mName.setText(model.getName());
           holder.mTime.setText(tss);
           holder.mMessage.setText(model.getMessage());
       } catch (Exception e)
       {
           e.printStackTrace();
       }

    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_chats_messages,parent,false);

        return new ChatHolder(view);
    }

    class ChatHolder extends RecyclerView.ViewHolder{
        TextView mName, mTime, mMessage;

        public ChatHolder(@NonNull View itemView) {
            super(itemView);
            mName=itemView.findViewById(R.id.memes_comment_username);
            mTime=itemView.findViewById(R.id.memes_comment_time);
            mMessage=itemView.findViewById(R.id.memes_comment_comment);
        }
    }
}

