package com.thedykewholovesswords.queertessential.RecycleViewAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.model.blog_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BlogRvAdapter  extends FirestoreRecyclerAdapter<blog_model,BlogRvAdapter.BlogHolder>{

   public OnItemClickListener listener;
    public BlogRvAdapter(@NonNull FirestoreRecyclerOptions<blog_model> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull BlogHolder holder, int position, @NonNull blog_model model) {

        try
        {
            Date ts = model.getTimestamp().toDate();

            DateFormat df = new SimpleDateFormat("MMM dd, yyyy  HH:mm");
            String tss = df.format(ts);

            holder.mTime.setText(tss);
            holder.mAuthor.setText("By: "+model.getAuthor());
            holder.mTitle.setText(model.getTitle());
            holder.mContent.setText(model.getContent());
            Picasso.get().load(model.getImageURL()).into(holder.mImage);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @NonNull
    @Override
    public BlogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_blog_short_display,parent,false);
        return new BlogHolder(view);
    }

    public class BlogHolder extends RecyclerView.ViewHolder {

        TextView mTitle,mContent,mAuthor,mTime;
        ImageView mImage;


        public BlogHolder(@NonNull View itemView) {
            super(itemView);

            mTitle=itemView.findViewById(R.id.blog_shot_Title);
            mContent=itemView.findViewById(R.id.blog_short_content);
            mAuthor=itemView.findViewById(R.id.blog_short_author);
            mTime=itemView.findViewById(R.id.blog_short_timestamp);
            mImage=itemView.findViewById(R.id.blog_shot_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position=getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION && listener!=null)
                    {
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }

                }
            });


        }
    }
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot,int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener=listener;

    }

}
