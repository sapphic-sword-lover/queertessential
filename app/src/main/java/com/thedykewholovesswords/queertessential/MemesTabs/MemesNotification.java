package com.thedykewholovesswords.queertessential.MemesTabs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.MemesComments;
import com.thedykewholovesswords.queertessential.MemesFullView;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.MemesRvAdapter;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.MyMemesRvAdapter;
import com.thedykewholovesswords.queertessential.model.memes_model;

import java.util.HashMap;
import java.util.Map;


public class MemesNotification extends Fragment {

    private FirebaseFirestore fstore;
    private MyMemesRvAdapter adapter;
    private FirebaseAuth fAuth;
    Dialog dialog;



    public MemesNotification() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View V=inflater.inflate(R.layout.fragment_memes_notification, container, false);
        fstore= FirebaseFirestore.getInstance();
        fAuth=FirebaseAuth.getInstance();
        dialog=new Dialog(getContext());
        setUpRecyclerView(V);
        return V;
    }
    private void setUpRecyclerView(View V) {

        String UserID = fAuth.getCurrentUser().getUid();


        Query query = fstore.collection("memes").whereEqualTo("uid",fAuth.getCurrentUser().getUid()).orderBy("timestamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<memes_model> options = new FirestoreRecyclerOptions.Builder<memes_model>().setQuery(query, memes_model.class).build();
        adapter = new MyMemesRvAdapter(options);
        RecyclerView recyclerView = V.findViewById(R.id.memes_myposts_rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new MyMemesRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                memes_model MM=documentSnapshot.toObject(memes_model.class);
                String id=documentSnapshot.getId();
                String imageUrl=MM.getImageURL();
                Intent intent=new Intent(getContext(), MemesFullView.class);
                Bundle bundle=new Bundle();
                bundle.putString("ID",id);
                bundle.putString("ImageURL",imageUrl);
                intent.putExtras(bundle);

                startActivity(intent);
            }

            @Override
            public void onLikeClick(DocumentSnapshot documentSnapshot, int position, ImageButton Like, TextView LikeCount) {
                fstore.collection("memes").document(documentSnapshot.getId()).collection("likes").document(UserID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                        if(!task.getResult().exists())
                        {
                            Map<String, Object>likesmap=new HashMap<>();
                            likesmap.put("timestamp", FieldValue.serverTimestamp());

                            fstore.collection("memes").document(documentSnapshot.getId()).collection("likes").document(UserID).set(likesmap);

                        }
                        else
                        {
                            fstore.collection("memes").document(documentSnapshot.getId()).collection("likes").document(UserID).delete();


                        }


                    }
                });
            }

            @Override
            public void onCommentClick(DocumentSnapshot documentSnapshot, int position) {
                memes_model MM=documentSnapshot.toObject(memes_model.class);
                String id=documentSnapshot.getId();
                Intent intent=new Intent(getContext(), MemesComments.class);
                Bundle bundle=new Bundle();
                bundle.putString("PostID",id);
                intent.putExtras(bundle);

                startActivity(intent);

            }

            @Override
            public void onDeleteClick(DocumentSnapshot documentSnapshot, int position) {
               // Toast.makeText(getContext(), "Delete Clicked", Toast.LENGTH_SHORT).show();
                open_confirmation_dialog(documentSnapshot.getId(),V);

            }
        });



    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
    void open_confirmation_dialog(String KEY,View view){
        dialog.setContentView(R.layout.dialog_confirmation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button mYes,mNo;
        mYes=dialog.findViewById(R.id.dialog_conf_yes);
        mNo=dialog.findViewById(R.id.dialog_conf_no_btn);
        dialog.setCanceledOnTouchOutside(false);
        mNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fstore.collection("memes").document(KEY).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(), "Item Deleted", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        setUpRecyclerView(view);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Error! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });

        dialog.show();

    }
}