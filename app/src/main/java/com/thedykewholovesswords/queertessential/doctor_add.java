package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.thedykewholovesswords.queertessential.model.doctors_model;

public class doctor_add extends AppCompatActivity {


    EditText mName,mSpeciality,mClinic,mPh,mAddress,mState;
    Button mSubmit;
    FirebaseFirestore fStore;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_add);
        getSupportActionBar().setTitle("Add To List of Doctors");

        mName=findViewById(R.id.doc_add_name);
        mPh=findViewById(R.id.doc_add_ph);
        mSpeciality=findViewById(R.id.doc_add_spec);
        mClinic=findViewById(R.id.doc_add_clinic);
        mAddress=findViewById(R.id.doc_add_add);
        mState=findViewById(R.id.doc_add_state);

        mSubmit=findViewById(R.id.doc_add_submit);

        fStore=FirebaseFirestore.getInstance();

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name=mName.getText().toString().trim();
                String ph=mPh.getText().toString().trim();
                String Spec=mSpeciality.getText().toString().trim();
                String Clinic=mClinic.getText().toString().trim();
                String Add=mAddress.getText().toString().trim();
                String State=mState.getText().toString().trim();
                onBackPressed();





                if(TextUtils.isEmpty(Name))
                {
                    mName.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(ph))
                {
                    mPh.setError("Field Mandatory");
                    return;
                }
                else if(ph.length()<9 || ph.length()>11)
                {
                    mPh.setError("Phone Number must be 10 digit or 11 Digit");
                    return;
                }

                if(TextUtils.isEmpty(Spec))
                {
                    mSpeciality.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(Clinic))
                {
                    mClinic.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(Add))
                {
                    mAddress.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(State))
                {
                    mState.setError("Field Mandatory");
                    return;
                }


                doctors_model dm=new doctors_model();
                dm.setName(Name);
                dm.setPhone_no(ph);
                dm.setSpeciality(Spec);
                dm.setClinic(Clinic);
                dm.setAddress(Add);
                dm.setState(State);

                DocumentReference documentReference=fStore.collection("doctors").document();

                documentReference.set(dm).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "Entry Added Successfully", Toast.LENGTH_SHORT).show();



                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("add123",""+e);
                    }
                });



            }
        });




    }
}