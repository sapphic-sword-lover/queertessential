package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter_LifecycleAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.model.doctors_model;
import com.thedykewholovesswords.queertessential.model.psychiatrist_model;

public class psychiatrists extends AppCompatActivity {

    private RecyclerView mPsychiatrist_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psychiatrists);
        getSupportActionBar().setTitle("Psychiatrists");


        mPsychiatrist_list=findViewById(R.id.psychiatrist_list_rv);

        fStore=FirebaseFirestore.getInstance();

        //Query
        Query query= fStore.collection("psychiatrists").orderBy("state");

        //Recyler handler

        FirestoreRecyclerOptions<psychiatrist_model> options= new FirestoreRecyclerOptions.Builder<psychiatrist_model>().setQuery(query,psychiatrist_model.class).build();

        adapter= new FirestoreRecyclerAdapter<psychiatrist_model, psychiatristsViewHolder>(options) {
            @NonNull
            @Override
            public psychiatristsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.psychiatrist_list_layout,parent,false);



                return new psychiatristsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull psychiatristsViewHolder holder, int position, @NonNull psychiatrist_model model) {
                holder.list_name.setText(model.getName());
                holder.list_ph.setText(model.getPhone_no());
                holder.list_clinic.setText(model.getClinic());
                holder.list_address.setText(model.getAddress());
                holder.list_state.setText(model.getState());
                if(model.isVerified()==true)
                {
                    holder.list_verified.setVisibility(View.VISIBLE);
                }
            }
        };



        mPsychiatrist_list.setHasFixedSize(true);
        mPsychiatrist_list.setLayoutManager(new LinearLayoutManager(this));
        mPsychiatrist_list.setAdapter(adapter);






    }

    public void open_addpsychiatrist(View view) {

        startActivity(new Intent(getApplicationContext(),psy_add.class));
    }

    private class psychiatristsViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_ph, list_clinic,list_address, list_state;
        ImageView list_verified;


        public psychiatristsViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.psychiatrist_lay_name);
            list_ph=itemView.findViewById(R.id.psychiatrist_lay_ph);
            list_clinic=itemView.findViewById(R.id.psychiatrist_lay_clinic);
            list_address=itemView.findViewById(R.id.psychiatrist_lay_add);
            list_state=itemView.findViewById(R.id.psychiatrist_lay_state);
            list_verified=itemView.findViewById(R.id.psychiatrist_verified_imageview);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_menu,menu);

        MenuItem item=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                processSearch(s);
                return false;


            }

            @Override
            public boolean onQueryTextChange(String s) {
                processSearch(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void processSearch(String s) {

        if(TextUtils.isEmpty(s))
        {
            //Query
            Query query= fStore.collection("psychiatrists").orderBy("state");

            //Recyler handler

            FirestoreRecyclerOptions<psychiatrist_model> options= new FirestoreRecyclerOptions.Builder<psychiatrist_model>().setQuery(query,psychiatrist_model.class).build();

            adapter= new FirestoreRecyclerAdapter<psychiatrist_model, psychiatristsViewHolder>(options) {
                @NonNull
                @Override
                public psychiatristsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.psychiatrist_list_layout,parent,false);



                    return new psychiatristsViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull psychiatristsViewHolder holder, int position, @NonNull psychiatrist_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_ph.setText(model.getPhone_no());
                    holder.list_clinic.setText(model.getClinic());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mPsychiatrist_list.setHasFixedSize(true);
            mPsychiatrist_list.setLayoutManager(new LinearLayoutManager(this));
            mPsychiatrist_list.setAdapter(adapter);
            adapter.startListening();

        } else
        {
            //Query
            Query query= fStore.collection("psychiatrists").orderBy("state").whereEqualTo("state",s);

            //Recyler handler

            FirestoreRecyclerOptions<psychiatrist_model> options= new FirestoreRecyclerOptions.Builder<psychiatrist_model>().setQuery(query,psychiatrist_model.class).build();

            adapter= new FirestoreRecyclerAdapter<psychiatrist_model, psychiatristsViewHolder>(options) {
                @NonNull
                @Override
                public psychiatristsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.psychiatrist_list_layout,parent,false);



                    return new psychiatristsViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull psychiatristsViewHolder holder, int position, @NonNull psychiatrist_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_ph.setText(model.getPhone_no());
                    holder.list_clinic.setText(model.getClinic());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mPsychiatrist_list.setHasFixedSize(true);
            mPsychiatrist_list.setLayoutManager(new LinearLayoutManager(this));
            mPsychiatrist_list.setAdapter(adapter);
            adapter.startListening();

        }

    }


}