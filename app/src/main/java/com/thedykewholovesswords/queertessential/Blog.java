package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.core.OrderBy;
import com.google.firebase.storage.FirebaseStorage;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.BlogRvAdapter;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.ChatRvAdapter;
import com.thedykewholovesswords.queertessential.Swipe.MyPosts;
import com.thedykewholovesswords.queertessential.model.blog_model;
import com.thedykewholovesswords.queertessential.model.chat_message_model;

public class Blog extends AppCompatActivity {

    private FirebaseFirestore fstore;
    private BlogRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        getSupportActionBar().setTitle("Blog");
        fstore= FirebaseFirestore.getInstance();

        setUpRecyclerView();

    }

    private void setUpRecyclerView() {

        Query query=fstore.collection("blogs").orderBy("timestamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<blog_model> options=new FirestoreRecyclerOptions.Builder<blog_model>().setQuery(query,blog_model.class).build();
        adapter=new BlogRvAdapter(options);
        RecyclerView recyclerView=findViewById(R.id.blog_list_rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new BlogRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                blog_model bmm=documentSnapshot.toObject(blog_model.class);
                String id=documentSnapshot.getId();
               // Toast.makeText(Blog.this, "Position "+position+" ID: "+id, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),BlogFullView.class);
                Bundle bundle=new Bundle();
                bundle.putString("ID",id);
                intent.putExtras(bundle);

                startActivity(intent);


            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
    adapter.stopListening();
    }

    public void open_blog_add(View view) {
        startActivity(new Intent(getApplicationContext(),blog_add.class));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.blog_menu,menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.my_posts:
                startActivity(new Intent(getApplicationContext(), MyPosts.class));

        }
        return super.onOptionsItemSelected(item);
    }
}