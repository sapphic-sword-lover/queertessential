package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.thedykewholovesswords.queertessential.model.ent_anime_model;
import com.thedykewholovesswords.queertessential.model.ent_games_model;
import com.thedykewholovesswords.queertessential.model.ent_movies_model;

import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.thedykewholovesswords.queertessential.model.ent_read_model;
import com.thedykewholovesswords.queertessential.model.ent_tv_model;

public class ent_add extends AppCompatActivity  {

    FirebaseFirestore fstore;
    TextInputLayout til_forms;
    AutoCompleteTextView act_form_dm;
    ArrayAdapter<String> adapter_forms;
    //FrameLayout f=(FrameLayout)findViewById(R.id.ent_add_form_frame);
    //LayoutInflater l=getLayoutInflater();
    //View v;

    TextInputLayout til_movies,til_tv,til_games,til_anime,til_read,til_read_type;
    AutoCompleteTextView act_movies_dm,act_tv_dm,act_anime_dm,act_read_dm,act_games_dm,act_read_type_dm;
    ArrayAdapter<String> adapter_movies,adapter_tv,adapter_anime,adapter_games,adapter_read,adapter_read_type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ent_add);
        getSupportActionBar().setTitle("Add to Entertainment");

        til_forms=findViewById(R.id.ent_add_form_til);
        act_form_dm=findViewById(R.id.movies_add_genre_dm);
        act_form_dm.setInputType(0);

        fstore=FirebaseFirestore.getInstance();




        adapter_forms=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.Ent_form));
        act_form_dm.setAdapter(adapter_forms);

        act_form_dm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text=act_form_dm.getText().toString();
                //Toast.makeText(ent_add.this,""+text,Toast.LENGTH_SHORT).show();
                FrameLayout f=(FrameLayout)findViewById(R.id.ent_add_form_frame);
                LayoutInflater l=getLayoutInflater();
                View v;

               if(text.matches("Movies"))
                {

                    int movie_year;

                    EditText mName;
                    EditText mStryear;
                    Button mSubmit;

                    f.removeAllViews();
                    v=l.inflate(R.layout.layout_ent_add_movies,null);
                    f.addView(v);
                    til_movies=v.findViewById(R.id.ent_add_movie_genre_til);
                    act_movies_dm=v.findViewById(R.id.movies_add_genre_dm);
                    act_movies_dm.setInputType(0);

                    adapter_movies=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.movies_genre));
                    act_movies_dm.setAdapter(adapter_movies);
                    mName=v.findViewById(R.id.movie_add_name);
                    mStryear=v.findViewById(R.id.movies_add_year);
                    mSubmit=v.findViewById(R.id.movies_add_button);

                    mSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String movie_genre=act_movies_dm.getText().toString();
                            String movie_name=mName.getText().toString().trim();
                            String Stryear=mStryear.getText().toString().trim();

                            if(TextUtils.isEmpty(Stryear))
                            {
                                mStryear.setError("Required Field");
                                return;

                            }

                            if(movie_genre.matches("Select Genre"))
                            {
                                Toast.makeText(ent_add.this,"Please Select a Genre",Toast.LENGTH_SHORT).show();
                                return;
                            }


                           if(TextUtils.isEmpty(movie_name))
                            {
                                mName.setError("Required Field");
                                return;
                            }


                            int movie_year = Integer.parseInt(Stryear);
                            if(movie_year<1800)
                            {
                                mStryear.setError("Invalid Year");
                                return;
                            }
                            onBackPressed();





                           //Toast.makeText(ent_add.this, "" + movie_name + "," + movie_genre + "," + movie_year, Toast.LENGTH_SHORT).show();
                            ent_movies_model mv=new ent_movies_model(movie_name,movie_genre,movie_year);
                            DocumentReference mvDR=fstore.collection("movies").document();
                            mvDR.set(mv).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(ent_add.this, "Entry Added Successfully", Toast.LENGTH_SHORT).show();
                                    f.removeAllViews();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(ent_add.this, "Error!"+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });




                        }
                    });





                }
               else if(text.matches("Tv Series"))
               {
                   EditText tName;
                   EditText tStryear;
                   Button tSubmit;
                   f.removeAllViews();
                   v=l.inflate(R.layout.layout_ent_add_tv,null);
                   f.addView(v);
                   til_tv=v.findViewById(R.id.ent_add_tv_genre_til);
                   act_tv_dm=v.findViewById(R.id.tv_add_genre_dm);
                   act_tv_dm.setInputType(0);

                   adapter_tv=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.movies_genre));
                   act_tv_dm.setAdapter(adapter_tv);

                   tName=v.findViewById(R.id.tv_add_name);
                   tStryear=v.findViewById(R.id.tv_add_year);
                   tSubmit=v.findViewById(R.id.tv_add_button);

                   tSubmit.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           String tv_genre=act_tv_dm.getText().toString();
                           String tv_name=tName.getText().toString().trim();
                           String tvStryear=tStryear.getText().toString().trim();


                           if(TextUtils.isEmpty(tvStryear))
                           {
                               tStryear.setError("Required Field");
                               return;

                           }

                           if(tv_genre.matches("Select Genre"))
                           {
                               Toast.makeText(ent_add.this,"Please Select a Genre",Toast.LENGTH_SHORT).show();
                               return;
                           }


                           if(TextUtils.isEmpty(tv_name))
                           {
                               tName.setError("Required Field");
                               return;
                           }


                           int tv_year = Integer.parseInt(tvStryear);
                           if(tv_year<1800)
                           {
                               tStryear.setError("Invalid Year");
                               return;
                           }
                           onBackPressed();





                           //Toast.makeText(ent_add.this, "" + movie_name + "," + movie_genre + "," + movie_year, Toast.LENGTH_SHORT).show();
                           ent_tv_model tv=new ent_tv_model(tv_name,tv_genre,tv_year);
                           DocumentReference tvDR=fstore.collection("tv").document();
                           tvDR.set(tv).addOnSuccessListener(new OnSuccessListener<Void>() {
                               @Override
                               public void onSuccess(Void aVoid) {
                                   Toast.makeText(ent_add.this, "Entry Added Successfully", Toast.LENGTH_SHORT).show();
                                   f.removeAllViews();

                               }
                           }).addOnFailureListener(new OnFailureListener() {
                               @Override
                               public void onFailure(@NonNull Exception e) {
                                   Toast.makeText(ent_add.this, "Error!"+e.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           });




                       }
                   });


               }
               else if(text.matches("Anime"))
               {
                   EditText aName;
                   EditText aStryear;
                   Button aSubmit;
                   f.removeAllViews();
                   v=l.inflate(R.layout.layout_ent_add_anime,null);
                   f.addView(v);
                   til_anime=v.findViewById(R.id.ent_add_anime_genre_til);
                   act_anime_dm=v.findViewById(R.id.anime_add_genre_dm);
                   act_anime_dm.setInputType(0);

                   adapter_anime=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.anime_genre));
                   act_anime_dm.setAdapter(adapter_anime);



                   aName=v.findViewById(R.id.anime_add_name);
                   aStryear=v.findViewById(R.id.anime_add_year);
                   aSubmit=v.findViewById(R.id.anime_add_button);

                   aSubmit.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           String anime_genre=act_anime_dm.getText().toString();
                           String anime_name=aName.getText().toString().trim();
                           String anime_Stryear=aStryear.getText().toString().trim();


                           if(TextUtils.isEmpty(anime_Stryear))
                           {
                               aStryear.setError("Required Field");
                               return;

                           }

                           if(anime_genre.matches("Select Genre"))
                           {
                               Toast.makeText(ent_add.this,"Please Select a Genre",Toast.LENGTH_SHORT).show();
                               return;
                           }


                           if(TextUtils.isEmpty(anime_name))
                           {
                               aName.setError("Required Field");
                               return;
                           }


                           int anime_year = Integer.parseInt(anime_Stryear);
                           if(anime_year<1800)
                           {
                               aStryear.setError("Invalid Year");
                               return;
                           }
                           onBackPressed();





                           //Toast.makeText(ent_add.this, "" + movie_name + "," + movie_genre + "," + movie_year, Toast.LENGTH_SHORT).show();
                           ent_anime_model an=new ent_anime_model(anime_name,anime_genre,anime_year);
                           DocumentReference anDR=fstore.collection("anime").document();
                           anDR.set(an).addOnSuccessListener(new OnSuccessListener<Void>() {
                               @Override
                               public void onSuccess(Void aVoid) {
                                   Toast.makeText(ent_add.this, "Entry Added Successfully", Toast.LENGTH_SHORT).show();
                                   f.removeAllViews();

                               }
                           }).addOnFailureListener(new OnFailureListener() {
                               @Override
                               public void onFailure(@NonNull Exception e) {
                                   Toast.makeText(ent_add.this, "Error!"+e.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           });




                       }
                   });

               }
               else if(text.matches("Games"))
               {
                   EditText gName;
                   EditText gStryear;
                   Button gSubmit;
                   f.removeAllViews();
                   v=l.inflate(R.layout.layout_ent_add_games,null);
                   f.addView(v);
                   til_games=v.findViewById(R.id.ent_add_games_genre_til);
                   act_games_dm=v.findViewById(R.id.games_add_genre_dm);
                   act_games_dm.setInputType(0);

                   adapter_games=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.games_genre));
                   act_games_dm.setAdapter(adapter_games);


                   gName=v.findViewById(R.id.read_add_name);
                   gStryear=v.findViewById(R.id.games_add_year);
                   gSubmit=v.findViewById(R.id.games_add_button);

                   gSubmit.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           String games_genre=act_games_dm.getText().toString();
                           String games_name=gName.getText().toString().trim();
                           String games_Stryear=gStryear.getText().toString().trim();
                           onBackPressed();

                           if(TextUtils.isEmpty(games_Stryear))
                           {
                               gStryear.setError("Required Field");
                               return;

                           }

                           if(games_genre.matches("Select Genre"))
                           {
                               Toast.makeText(ent_add.this,"Please Select a Genre",Toast.LENGTH_SHORT).show();
                               return;
                           }


                           if(TextUtils.isEmpty(games_name))
                           {
                               gName.setError("Required Field");
                               return;
                           }


                           int games_year = Integer.parseInt(games_Stryear);
                           if(games_year<1800)
                           {
                               gStryear.setError("Invalid Year");
                               return;
                           }
                           onBackPressed();





                           //Toast.makeText(ent_add.this, "" + movie_name + "," + movie_genre + "," + movie_year, Toast.LENGTH_SHORT).show();
                           ent_games_model g=new ent_games_model(games_name,games_genre,games_year);
                           DocumentReference gDR=fstore.collection("games").document();
                           gDR.set(g).addOnSuccessListener(new OnSuccessListener<Void>() {
                               @Override
                               public void onSuccess(Void aVoid) {
                                   Toast.makeText(ent_add.this, "Entry Added Successfully", Toast.LENGTH_SHORT).show();
                                   f.removeAllViews();

                               }
                           }).addOnFailureListener(new OnFailureListener() {
                               @Override
                               public void onFailure(@NonNull Exception e) {
                                   Toast.makeText(ent_add.this, "Error!"+e.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           });




                       }
                   });

               }
               else if(text.matches("Reading Materials"))
               {
                   EditText rName;
                   EditText rStryear;
                   Button rSubmit;

                   f.removeAllViews();
                   v=l.inflate(R.layout.layout_ent_add_read,null);
                   f.addView(v);
                   til_read=v.findViewById(R.id.ent_add_read_genre_til);
                   act_read_dm=v.findViewById(R.id.read_add_genre_dm);
                   act_read_dm.setInputType(0);

                   adapter_read=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.read_genre));
                   act_read_dm.setAdapter(adapter_read);

                   til_read_type=v.findViewById(R.id.ent_add_read_type_til);
                   act_read_type_dm=v.findViewById(R.id.read_add_type_dm);
                   act_read_type_dm.setInputType(0);

                   adapter_read_type=new ArrayAdapter<>(getApplicationContext(),R.layout.ent_add_item,getResources().getStringArray(R.array.read_type));
                   act_read_type_dm.setAdapter(adapter_read_type);




                   rName=v.findViewById(R.id.read_add_name);
                   rStryear=v.findViewById(R.id.read_add_year);
                   rSubmit=v.findViewById(R.id.read_add_button);

                   rSubmit.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           String read_genre=act_read_dm.getText().toString();
                           String read_type=act_read_type_dm.getText().toString();
                           String read_name=rName.getText().toString().trim();
                           String read_Stryear=rStryear.getText().toString().trim();


                           if(TextUtils.isEmpty(read_Stryear))
                           {
                               rStryear.setError("Required Field");
                               return;

                           }

                           if(read_genre.matches("Select Genre"))
                           {
                               Toast.makeText(ent_add.this,"Please Select a Genre",Toast.LENGTH_SHORT).show();
                               return;
                           }
                           if(read_type.matches("Select Type"))
                           {
                               Toast.makeText(ent_add.this,"Please Select a Type",Toast.LENGTH_SHORT).show();
                               return;
                           }


                           if(TextUtils.isEmpty(read_name))
                           {
                               rName.setError("Required Field");
                               return;
                           }


                           int read_year = Integer.parseInt(read_Stryear);
                           if(read_year<1800)
                           {
                               rStryear.setError("Invalid Year");
                               return;
                           }
                           onBackPressed();





                           //Toast.makeText(ent_add.this, "" + movie_name + "," + movie_genre + "," + movie_year, Toast.LENGTH_SHORT).show();
                           ent_read_model r=new ent_read_model(read_name,read_type,read_genre,read_year);
                           DocumentReference rDR=fstore.collection("reads").document();
                           rDR.set(r).addOnSuccessListener(new OnSuccessListener<Void>() {
                               @Override
                               public void onSuccess(Void aVoid) {
                                   Toast.makeText(ent_add.this, "Entry Added Successfully", Toast.LENGTH_SHORT).show();
                                   f.removeAllViews();

                               }
                           }).addOnFailureListener(new OnFailureListener() {
                               @Override
                               public void onFailure(@NonNull Exception e) {
                                   Toast.makeText(ent_add.this, "Error!"+e.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           });




                       }
                   });





               }
               else
               {   f.removeAllViews();
                   
               }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

}