package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.thedykewholovesswords.queertessential.model.lawyer_model;
import com.thedykewholovesswords.queertessential.model.ss_model;

public class ss_add extends AppCompatActivity {

    EditText mName, mAddress, mState;
    Button mSubmit;
    FirebaseFirestore fStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ss_add);
        getSupportActionBar().setTitle("Add To List of Safe Spaces");

        mName = findViewById(R.id.ss_add_name);
        mAddress = findViewById(R.id.ss_add_add);
        mState = findViewById(R.id.ss_add_state);

        mSubmit = findViewById(R.id.ss_add_submit);

        fStore = FirebaseFirestore.getInstance();


        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = mName.getText().toString().trim();
                String Add = mAddress.getText().toString().trim();
                String State = mState.getText().toString().trim();
                onBackPressed();


                if (TextUtils.isEmpty(Name)) {
                    mName.setError("Field Mandatory");
                    return;
                }


                if (TextUtils.isEmpty(Add)) {
                    mAddress.setError("Field Mandatory");
                    return;
                }

                if (TextUtils.isEmpty(State)) {
                    mState.setError("Field Mandatory");
                    return;
                }


                ss_model dm = new ss_model(Name, Add, State);

                DocumentReference documentReference = fStore.collection("safe_spaces").document();

                documentReference.set(dm).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "Entry Added Successfully", Toast.LENGTH_SHORT).show();




                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("add123", "" + e);
                    }
                });


            }
        });
    }
}