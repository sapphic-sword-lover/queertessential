package com.thedykewholovesswords.queertessential.EventsTabs;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.EventsFullPosterView;
import com.thedykewholovesswords.queertessential.R;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.EventsRvAdapter;
import com.thedykewholovesswords.queertessential.model.Events_model;

import java.util.Calendar;
import java.util.Date;


public class EventsPast extends Fragment {

    FirebaseFirestore fStore;
    EventsRvAdapter adapter;
    Timestamp timestampNow;
    Calendar cal,cal1;



    public EventsPast() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View V=inflater.inflate(R.layout.fragment_events_past, container, false);

        fStore=FirebaseFirestore.getInstance();
        cal=Calendar.getInstance();
        cal1=Calendar.getInstance();
        int DAY=cal1.get(Calendar.DATE);
        int MONTH=cal1.get(Calendar.MONTH);
        int YEAR=cal1.get(Calendar.YEAR);
        int HOUR=cal1.get(Calendar.HOUR_OF_DAY);
        int MIN=cal1.get(Calendar.MINUTE);


        cal.set(Calendar.DATE,DAY);
        cal.set(Calendar.MONTH,MONTH);
        cal.set(Calendar.YEAR,YEAR);
        cal.set(Calendar.HOUR_OF_DAY,HOUR);
        cal.set(Calendar.MINUTE,MIN);
        try {
            Date date=cal.getTime();
            timestampNow=new Timestamp(date);
        }catch (Exception e)
        {
            e.printStackTrace();
        }



       setUpRecyclerView(V,timestampNow);


        return V;
    }

    private void setUpRecyclerView(View v, Timestamp timestampNow) {

        Query query=fStore.collection("events").whereLessThan("eventTimeStamp",timestampNow).orderBy("eventTimeStamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<Events_model> options=new FirestoreRecyclerOptions.Builder<Events_model>().setQuery(query,Events_model.class).build();
        adapter=new EventsRvAdapter(options);
        RecyclerView recyclerView=v.findViewById(R.id.events_past_rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new EventsRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                Events_model EM=documentSnapshot.toObject(Events_model.class);
                String id=documentSnapshot.getId();
                String imageUrl=EM.getEventPosterURL();
                Intent intent=new Intent(getContext(), EventsFullPosterView.class);
                Bundle bundle=new Bundle();
                bundle.putString("ID",id);
                bundle.putString("ImageURL",imageUrl);
                intent.putExtras(bundle);

                startActivity(intent);

                // Toast.makeText(getContext(), ""+documentSnapshot.getId()+" "+EM.getEventName(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


}