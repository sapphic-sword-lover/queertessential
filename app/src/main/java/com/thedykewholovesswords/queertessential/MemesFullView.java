package com.thedykewholovesswords.queertessential;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firestore.v1.StructuredQuery;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.CommentsRvAdapter;
import com.thedykewholovesswords.queertessential.UserInformation.UserRetriver;
import com.thedykewholovesswords.queertessential.UserInformation.User_model;
import com.thedykewholovesswords.queertessential.model.comments_model;
import com.thedykewholovesswords.queertessential.model.memes_model;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MemesFullView extends AppCompatActivity {

    String PostID,ImageURL;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    PhotoView mImage;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memes_full_view);
        getSupportActionBar().hide();


        Bundle bundle=getIntent().getExtras();
        PostID=bundle.getString("ID");
        ImageURL=bundle.getString("ImageURL");

        mImage=findViewById(R.id.memes_full_image);

        Picasso.get().load(ImageURL).into(mImage);


    }


}