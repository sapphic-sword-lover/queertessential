package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.model.lawyer_model;
import com.thedykewholovesswords.queertessential.model.sph_model;

import de.hdodenhof.circleimageview.CircleImageView;

public class sph extends AppCompatActivity {

    private RecyclerView mSph_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sph);
        getSupportActionBar().setTitle("Suicide Prevention Hotlines");

        mSph_list=findViewById(R.id.sph_list_rv);

        fStore=FirebaseFirestore.getInstance();

        //Query
        Query query= fStore.collection("sph").orderBy("name");

        //Recyler handler

        FirestoreRecyclerOptions<sph_model> options= new FirestoreRecyclerOptions.Builder<sph_model>().setQuery(query,sph_model.class).build();

        adapter= new FirestoreRecyclerAdapter<sph_model, sphViewHolder>(options) {
            @NonNull
            @Override
            public sphViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.sph_list_layout,parent,false);



                return new sphViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull sphViewHolder holder, int position, @NonNull sph_model model) {
                holder.list_name.setText(model.getName());
                holder.list_ph.setText(model.getPhone_no());
                if(model.isVerified()==true)
                {
                    holder.list_verified.setVisibility(View.VISIBLE);
                }
               

            }
        };



        mSph_list.setHasFixedSize(true);
        mSph_list.setLayoutManager(new LinearLayoutManager(this));
        mSph_list.setAdapter(adapter);


    }

    public void open_addSph(View view) {
     startActivity(new Intent(getApplicationContext(),sph_add.class));

    }

    private class sphViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_ph;
        CircleImageView logo;
        ImageView list_verified;


        public sphViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.sph_lay_name);
            list_ph=itemView.findViewById(R.id.sph_lay_ph);
            logo=itemView.findViewById(R.id.sph_logo);
            list_verified=itemView.findViewById(R.id.sph_verified_imageview);


        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
}