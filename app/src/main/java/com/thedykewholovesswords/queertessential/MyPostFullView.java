package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.Swipe.MyPosts;
import com.thedykewholovesswords.queertessential.model.blog_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyPostFullView extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    FirebaseFirestore fStore;
    String ID;
    TextView mTitle,mAuthor,mContent,mTime;
    ImageView mImageview;
    ImageButton MyPost_MENU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post_full_view);
        getSupportActionBar().hide();

        fStore=FirebaseFirestore.getInstance();

        mTitle=findViewById(R.id.MP_full_title);
        mAuthor=findViewById(R.id.MP_full_author);
        mTime=findViewById(R.id.MP_full_time);
        mContent=findViewById(R.id.MP_full_content);

        mImageview=findViewById(R.id.MP_full_image);



        Bundle bundle=getIntent().getExtras();
        ID=bundle.getString("ID");

        DocumentReference DR=fStore.collection("blogs").document(ID);
        DR.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists())
                {
                    try {
                        blog_model bm=documentSnapshot.toObject(blog_model.class);
                        Date ts = bm.getTimestamp().toDate();

                        DateFormat df = new SimpleDateFormat("MMM dd, yyyy  HH:mm");
                        String tss = df.format(ts);

                        mTitle.setText(bm.getTitle());
                        Picasso.get().load(bm.getImageURL()).into(mImageview);
                        mAuthor.setText("By: "+bm.getAuthor());
                        mTime.setText(tss);
                        mContent.setText(bm.getContent());




                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }





                }else {
                    Toast.makeText(MyPostFullView.this, "Document Missing", Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MyPostFullView.this, "Error! "+e.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });



    }
    public void showPopUpMenu(View v)
    {
        PopupMenu popupMenu=new PopupMenu(this,v);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.my_posts_menu);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.myposts_del:
                DeletePost(ID);
                startActivity(new Intent(getApplicationContext(), MyPosts.class));
                //Toast.makeText(this, "Delete Clicked "+ ID, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.myposts_edit:
                //Toast.makeText(this, "Edit Clicked "+ ID, Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(getApplicationContext(),MyPostEdit.class);
                Bundle bundle=new Bundle();
                bundle.putString("ID",ID);
                intent.putExtras(bundle);

                startActivity(intent);
                return true;

        }
        return false;
    }

    public void DeletePost(String PostID)
    {
        DocumentReference documentReference=fStore.collection("blogs").document(PostID);
        documentReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(MyPostFullView.this, "The Blog Post Was Deleted", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MyPostFullView.this, "Error! "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}