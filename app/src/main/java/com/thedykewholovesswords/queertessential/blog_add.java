package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.model.blog_model;

public class blog_add extends AppCompatActivity {

    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    FirebaseStorage fCloud;
    EditText mAuthor,mTitle,mContent;
    ImageView mBlogImage;
    Button mPost;
    Dialog dialog;
    LottieAnimationView load;

    StorageReference SR;
    DocumentReference DR;

    private  static final int PICK_IMAGE_REQUEST=1;
    private Uri mImageURI;
    static String dlURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_add);
        getSupportActionBar().setTitle("New Post");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        fAuth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();
        fCloud=FirebaseStorage.getInstance();

        mAuthor=findViewById(R.id.memes_add_name);
        mTitle=findViewById(R.id.blog_add_title);
        mContent=findViewById(R.id.memes_add_caption);

        mBlogImage=findViewById(R.id.meme_add_image);
        mPost=findViewById(R.id.memes_add_post_btn);

        dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);



        mBlogImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilePicker();

            }
        });

        SR=fCloud.getReference("Blog_Images/Images");
        DR=fStore.collection("blogs").document();

        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uploadData();

            }
        });


    }

    private String getFileExtension(Uri uri)
    {
        ContentResolver CR=getContentResolver();
        MimeTypeMap mime=MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(CR.getType(uri));
    }

    private void uploadData()
    {

        String Title=mTitle.getText().toString();
        String Content=mContent.getText().toString();
        String Author=mAuthor.getText().toString();


        if(mImageURI==null)
        {
            Toast.makeText(this, "Error! Select an Image", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(Author))
        {
            mAuthor.setError("Required Field");
            return;
        }
        if(TextUtils.isEmpty(Title))
        {
            mTitle.setError("Required Field");
            return;
        }
        if(TextUtils.isEmpty(Content))
        {
            mContent.setError("Required Field");
            return;
        }
        StorageReference fileReference=SR.child(System.currentTimeMillis()+"."+getFileExtension(mImageURI));
        fileReference.putFile(mImageURI)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                        fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Uri dlUri=uri;
                                dlURL=dlUri.toString();

                                blog_model bm=new blog_model();
                                bm.setAuthor(Author);
                                bm.setTitle(Title);
                                bm.setContent(Content);
                                bm.setImageURL(dlURL);
                                bm.setUID(fAuth.getCurrentUser().getUid().toString());

                                DR.set(bm).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {


                                        dialog.dismiss();
                                        mBlogImage.setImageDrawable(getDrawable(R.drawable.ic_baseline_add_photo_alternate_24));
                                        mAuthor.setText("");
                                        mTitle.setText("");
                                        mContent.setText("");
                                        Toast.makeText(blog_add.this, "Upload Successful", Toast.LENGTH_SHORT).show();
                                        onBackPressed();

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(blog_add.this, "Error! "+e.getMessage(), Toast.LENGTH_LONG).show();

                                    }
                                });


                            }
                        });





                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(blog_add.this, "Error! "+e.getMessage(), Toast.LENGTH_LONG).show();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {

                ProgressBar mProgress=dialog.findViewById(R.id.upload_progress);
                load = dialog.findViewById(R.id.loading_progress);
                load.setVisibility(View.VISIBLE);
                dialog.show();


            }
        });



    }

    private void openFilePicker() {
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICK_IMAGE_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {  try {
            mImageURI=data.getData();
            Picasso.get().load(mImageURI).into(mBlogImage);

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        }
    }
}