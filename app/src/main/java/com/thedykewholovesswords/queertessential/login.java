package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class login extends AppCompatActivity {
    EditText mEmail, mPassword;
    Button mLogIn;
    ProgressBar mprogressbar;
    FirebaseAuth fAuth;
    Dialog dialog;
    LottieAnimationView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        fAuth=FirebaseAuth.getInstance();
        dialog=new Dialog(this);


        mEmail=findViewById(R.id.reset_email);
        mPassword=findViewById(R.id.login_password);
        mprogressbar=findViewById(R.id.progressBar);
        mLogIn=findViewById(R.id.login_btn);
        loading = findViewById(R.id.loading_amim);




        if(fAuth.getCurrentUser()!=null)
        {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();

        }

        mLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString().trim();
                String pwd = mPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Email is Mandatory");
                    return;
                }

                if (TextUtils.isEmpty(pwd)) {
                    mPassword.setError("Password is Mandatory");
                    return;
                }
                mprogressbar.setVisibility(View.INVISIBLE);
                loading.setVisibility(View.VISIBLE);

                //authentication
                fAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(login.this, "Login Sucessfull", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                        else
                        {
                            Toast.makeText(login.this, "Error!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            mprogressbar.setVisibility(View.INVISIBLE);
                            loading.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        });


    }

    public void open_register(View view) {
        startActivity(new Intent(getApplicationContext(),register.class));
        finish();
    }

    public void open_reset_box(View view) {
        open_reset_dialog();
    }




    private void open_reset_dialog() {
        dialog.setContentView(R.layout.dialog_reset_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        EditText mEmail;
        Button mSend;
        mEmail=dialog.findViewById(R.id.reset_email);
        mSend=dialog.findViewById(R.id.reset_button);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailid=mEmail.getText().toString().trim();
                if (TextUtils.isEmpty(emailid))
                {
                    mEmail.setError("Required");
                    return;
                }
                fAuth.sendPasswordResetEmail(emailid).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(),"Password Reset Email Has Been Sent",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Toast.makeText(getApplicationContext(),"Error: "+e.getMessage(),Toast.LENGTH_SHORT).show();

                    }
                });



            }
        });


        dialog.show();


    }


}