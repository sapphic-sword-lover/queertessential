package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.model.shelter_model;

public class shelter extends AppCompatActivity {


    private RecyclerView mShelter_list;
    private FirebaseFirestore fStore;
    private FirestoreRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shelter);
        getSupportActionBar().setTitle("Shelters");


        mShelter_list=findViewById(R.id.shelters_list_rv);

        fStore=FirebaseFirestore.getInstance();

        Query query= fStore.collection("shelters").orderBy("state");

        FirestoreRecyclerOptions<shelter_model> options= new FirestoreRecyclerOptions.Builder<shelter_model>().setQuery(query,shelter_model.class).build();

        adapter= new FirestoreRecyclerAdapter<shelter_model, shelterViewHolder >(options) {
            @NonNull
            @Override
            public shelterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.shelters_list_layout,parent,false);



                return new shelterViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull shelterViewHolder holder, int position, @NonNull shelter_model model) {
                holder.list_name.setText(model.getName());
                holder.list_ph.setText(model.getPhone_no());
                holder.list_address.setText(model.getAddress());
                holder.list_state.setText(model.getState());
                if(model.isVerified()==true)
                {
                    holder.list_verified.setVisibility(View.VISIBLE);
                }
            }
        };



        mShelter_list.setHasFixedSize(true);
        mShelter_list.setLayoutManager(new LinearLayoutManager(this));
        mShelter_list.setAdapter(adapter);





    }

    public void open_addShelter(View view) {
        startActivity(new Intent(getApplicationContext(),shelter_add.class));
    }


    private class shelterViewHolder extends RecyclerView.ViewHolder{

        private TextView list_name,list_ph,list_address, list_state;
        ImageView list_verified;


        public shelterViewHolder(@NonNull View itemView) {
            super(itemView);

            list_name=itemView.findViewById(R.id.shelter_lay_name);
            list_ph=itemView.findViewById(R.id.shelter_lay_ph);
            list_address=itemView.findViewById(R.id.shelter_lay_add);
            list_state=itemView.findViewById(R.id.shelter_lay_state);
            list_verified=itemView.findViewById(R.id.shelter_verified_imageview);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_menu,menu);

        MenuItem item=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                processSearch(s);
                return false;


            }

            @Override
            public boolean onQueryTextChange(String s) {
                processSearch(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void processSearch(String s) {

        if(TextUtils.isEmpty(s))
        {
            Query query= fStore.collection("shelters").orderBy("state");

            FirestoreRecyclerOptions<shelter_model> options= new FirestoreRecyclerOptions.Builder<shelter_model>().setQuery(query,shelter_model.class).build();

            adapter= new FirestoreRecyclerAdapter<shelter_model, shelterViewHolder >(options) {
                @NonNull
                @Override
                public shelterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.shelters_list_layout,parent,false);



                    return new shelterViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull shelterViewHolder holder, int position, @NonNull shelter_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_ph.setText(model.getPhone_no());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mShelter_list.setHasFixedSize(true);
            mShelter_list.setLayoutManager(new LinearLayoutManager(this));
            mShelter_list.setAdapter(adapter);
            adapter.startListening();

        } else
        {
            Query query= fStore.collection("shelters").orderBy("state").whereEqualTo("state",s);

            FirestoreRecyclerOptions<shelter_model> options= new FirestoreRecyclerOptions.Builder<shelter_model>().setQuery(query,shelter_model.class).build();

            adapter= new FirestoreRecyclerAdapter<shelter_model, shelterViewHolder >(options) {
                @NonNull
                @Override
                public shelterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.shelters_list_layout,parent,false);



                    return new shelterViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull shelterViewHolder holder, int position, @NonNull shelter_model model) {
                    holder.list_name.setText(model.getName());
                    holder.list_ph.setText(model.getPhone_no());
                    holder.list_address.setText(model.getAddress());
                    holder.list_state.setText(model.getState());
                    if(model.isVerified()==true)
                    {
                        holder.list_verified.setVisibility(View.VISIBLE);
                    }
                }
            };



            mShelter_list.setHasFixedSize(true);
            mShelter_list.setLayoutManager(new LinearLayoutManager(this));
            mShelter_list.setAdapter(adapter);
            adapter.startListening();

        }

    }

}