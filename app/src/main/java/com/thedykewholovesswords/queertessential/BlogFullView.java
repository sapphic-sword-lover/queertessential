package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.model.blog_model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BlogFullView extends AppCompatActivity {

    FirebaseFirestore fStore;
    String ID;
    TextView mTitle,mAuthor,mContent,mTime;
    ImageView mImageview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_full_view);
        getSupportActionBar().hide();
        fStore=FirebaseFirestore.getInstance();

        mTitle=findViewById(R.id.blog_full_title);
        mAuthor=findViewById(R.id.blog_full_author);
        mTime=findViewById(R.id.blog_full_time);
        mContent=findViewById(R.id.blog_full_content);

        mImageview=findViewById(R.id.blog_full_image);


        Bundle bundle=getIntent().getExtras();
        ID=bundle.getString("ID");
        //Toast.makeText(this, ""+ID, Toast.LENGTH_SHORT).show();

        DocumentReference DR=fStore.collection("blogs").document(ID);
       DR.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
           @Override
           public void onSuccess(DocumentSnapshot documentSnapshot) {
               if(documentSnapshot.exists())
               {
                   try {
                       blog_model bm=documentSnapshot.toObject(blog_model.class);
                       Date ts = bm.getTimestamp().toDate();

                       DateFormat df = new SimpleDateFormat("MMM dd, yyyy  HH:mm");
                       String tss = df.format(ts);

                       mTitle.setText(bm.getTitle());
                       Picasso.get().load(bm.getImageURL()).into(mImageview);
                       mAuthor.setText("By: "+bm.getAuthor());
                       mTime.setText(tss);
                       mContent.setText(bm.getContent());




                   }catch (Exception e)
                   {
                       e.printStackTrace();
                   }





               }else {
                   Toast.makeText(BlogFullView.this, "Document Missing", Toast.LENGTH_SHORT).show();
               }

           }
       }).addOnFailureListener(new OnFailureListener() {
           @Override
           public void onFailure(@NonNull Exception e) {
               Toast.makeText(BlogFullView.this, "Error! "+e.getMessage(), Toast.LENGTH_SHORT).show();


           }
       });




    }
}