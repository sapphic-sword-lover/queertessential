package com.thedykewholovesswords.queertessential;

import android.R.color;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.thedykewholovesswords.queertessential.UserInformation.User_model;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    public String mDISP;
    FirebaseAuth fAuth;
    FirebaseFirestore fstore;
    String UID;
    View hView;
    TextView DisplayName;
    User_model um;
    Dialog dialog,dialog2,dialog3;
    Switch mLightEnableSwitch;
    SharedPreferences sharedPreferences;




    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dialog=new Dialog(this);
        dialog2=new Dialog(this);
        dialog3=new Dialog(this);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);








        fstore= FirebaseFirestore.getInstance();
        fAuth=FirebaseAuth.getInstance();

        UID=fAuth.getCurrentUser().getUid().toString();
        DocumentReference documentReference=fstore.collection("users").document(UID);

        documentReference.addSnapshotListener( new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {

                if (error != null) {
                    Log.w("abc123", "Listen failed.", error);
                    return;
                }

                if (value != null && value.exists()) {
                    //String DN=value.getString("dName");
                   // String Email=value.getString("email");
                    //mDISP.setText(value.getString("dName"));
                   // Log.d("abc123", "Current data: "+DN);
                    //um=new User_model(DN,Email);
                    NavigationView navigationView = findViewById(R.id.nav_view);
                    hView=navigationView.getHeaderView(0);
                    DisplayName=(TextView)hView.findViewById(R.id.DISP_NAME);
                    DisplayName.setText(value.getString("dName"));
                    Intent intent=new Intent(getApplicationContext(),ChatRoom.class);
                    intent.putExtra("dName",value.getString("dName"));



                } else {
                    Log.d("abc123", "Current data: null");
                }





            }
        });



        //getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(color.transparent)));
        
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        hView=navigationView.getHeaderView(0);
        navigationView.setItemIconTintList(null);
        DisplayName=(TextView)hView.findViewById(R.id.DISP_NAME);
        DisplayName.setText("");
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        ;

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void logout(MenuItem item) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), login.class));
        finish();
    }

    public void open_sph(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), sph.class));
        //finish();
    }

    public void open_doctors(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), doctors.class));
        //finish();
    }
    public void open_lawyers(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), lawyers.class));
        //finish();
    }
    public void open_ngo(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), ngo.class));
        //finish();
    }
    public void open_blog(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), Blog.class));
        //finish();
    }

    public void open_about(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), About.class));
        //finish();
    }

    public void open_psychiatrists(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), psychiatrists.class));
        //finish();
    }
    public void open_shelter(MenuItem item) {
        startActivity(new Intent(getApplicationContext(), shelter.class));
        //finish();
    }
    public void open_memes(MenuItem item) {
        startActivity(new Intent(getApplicationContext(),Memes.class));
    }
    public void   open_safeSpaces(MenuItem item){
        startActivity(new Intent(getApplicationContext(),safeSpaces.class));


           }
    public void open_qfc(MenuItem item){

        startActivity(new Intent(getApplicationContext(),QFC.class));


  }
    public void open_entertainment(MenuItem item){

        startActivity(new Intent(getApplicationContext(),entertainment.class));


    }
    public void open_chatroom(MenuItem item) {
        startActivity(new Intent(getApplicationContext(),ChatRoom.class));
    }
    public void open_events(MenuItem item){
        startActivity(new Intent(getApplicationContext(),Events.class));
    }


    public void reset_password(MenuItem item) {
        open_reset_dialog();
    }

    private void open_reset_dialog() {
        dialog.setContentView(R.layout.dialog_reset_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        EditText mEmail;
        Button mSend;
        mEmail=dialog.findViewById(R.id.reset_email);
        mSend=dialog.findViewById(R.id.reset_button);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailid=mEmail.getText().toString().trim();
                if (TextUtils.isEmpty(emailid))
                {
                    mEmail.setError("Required");
                    return;
                }
                fAuth.sendPasswordResetEmail(emailid).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(),"Password Reset Email Has Been Sent",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Toast.makeText(getApplicationContext(),"Error: "+e.getMessage(),Toast.LENGTH_SHORT).show();

                    }
                });



            }
        });


        dialog.show();


    }

    public void open_dialog_update_dn(View view) {
        //Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
        open_update_dialog();
    }

    private void open_update_dialog() {

        dialog2.setContentView(R.layout.dialog_update_dn);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        EditText mDN;
        Button mUpdate;
        mDN=dialog2.findViewById(R.id.dn_name_update);
        mUpdate=dialog2.findViewById(R.id.DN_update_button);

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String diName=mDN.getText().toString().trim();
                String UUID=fAuth.getCurrentUser().getUid();
                //Toast.makeText(MainActivity.this, ""+UUID, Toast.LENGTH_SHORT).show();
                DocumentReference documentReference=fstore.collection("users").document(UID);
                Map<String,Object> map=new HashMap<>();
                map.put("dName",diName);

                documentReference.update(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(MainActivity.this, "Display Name Updated", Toast.LENGTH_SHORT).show();
                        dialog2.dismiss();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });



            }
        });

        dialog2.show();




    }

    public void appearance_DB(MenuItem item) {
        open_appearnce_dialog();
    }

    private void open_appearnce_dialog() {

        dialog3.setContentView(R.layout.dialog_update_appearence);
        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog3.show();

        mLightEnableSwitch=dialog3.findViewById(R.id.appear_light_switch);






    }
}