package com.thedykewholovesswords.queertessential;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

public class EventsFullPosterView extends AppCompatActivity {
    String PostID,ImageURL;
    PhotoView mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_full_poster_view);
        getSupportActionBar().hide();


        Bundle bundle=getIntent().getExtras();
        PostID=bundle.getString("ID");
        ImageURL=bundle.getString("ImageURL");


        mImage=findViewById(R.id.events_full_poster);

        Picasso.get().load(ImageURL).into(mImage);
    }
}