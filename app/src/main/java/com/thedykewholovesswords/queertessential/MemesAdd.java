package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.thedykewholovesswords.queertessential.model.blog_model;
import com.thedykewholovesswords.queertessential.model.memes_model;

public class MemesAdd extends AppCompatActivity {

    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    FirebaseStorage fCloud;
    EditText mName,mCaption;
    ImageView mMemeImage;
    Button mPost;
    Dialog dialog;
    LottieAnimationView load;

    StorageReference SR;
    DocumentReference DR;

    private  static final int PICK_IMAGE_REQUEST=1;
    private Uri mImageURI;
    static String dlURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memes_add);
        getSupportActionBar().setTitle("New Post");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        fAuth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();
        fCloud=FirebaseStorage.getInstance();

        mName=findViewById(R.id.memes_add_name);
        mCaption=findViewById(R.id.memes_add_caption);

        mMemeImage=findViewById(R.id.meme_add_image);
        mPost=findViewById(R.id.memes_add_post_btn);

        dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);

        mMemeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilePicker();

            }
        });

        SR=fCloud.getReference("Meme_Images/Images");
        DR=fStore.collection("memes").document();

        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadData();
            }
        });


    }
    private String getFileExtension(Uri uri)
    {
        ContentResolver CR=getContentResolver();
        MimeTypeMap mime=MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(CR.getType(uri));
    }
    private void openFilePicker() {
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICK_IMAGE_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {  try {
            mImageURI=data.getData();
            Picasso.get().load(mImageURI).into(mMemeImage);

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        }
    }


    private void uploadData()
    {


        String Caption=mCaption.getText().toString();
        String Name=mName.getText().toString();

        if(mImageURI==null)
        {
            Toast.makeText(this, "Error! Select an Image", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(Name))
        {
            mName.setError("Required Field");
            return;
        }
        if(TextUtils.isEmpty(Caption))
        {
            mCaption.setError("Required Field");
            return;
        }
        if(Caption.length()>280)
        {
            mCaption.setError("Max. Length is 280 Characters");
            return;
        }
        StorageReference fileReference=SR.child(System.currentTimeMillis()+"."+getFileExtension(mImageURI));
        fileReference.putFile(mImageURI)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                        fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Uri dlUri=uri;
                                dlURL=dlUri.toString();

                                memes_model Mm=new memes_model();
                                Mm.setName(Name);
                                Mm.setCaption(Caption);
                                Mm.setImageURL(dlURL);
                                Mm.setUID(fAuth.getCurrentUser().getUid().toString());

                                DR.set(Mm).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {


                                        dialog.dismiss();
                                        mMemeImage.setImageDrawable(getDrawable(R.drawable.ic_baseline_add_photo_alternate_24));
                                        mName.setText("");
                                        mCaption.setText("");
                                        Toast.makeText(getApplicationContext(), "Upload Successful", Toast.LENGTH_SHORT).show();
                                        onBackPressed();

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(getApplicationContext(), "Error! "+e.getMessage(), Toast.LENGTH_LONG).show();

                                    }
                                });


                            }
                        });





                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error! "+e.getMessage(), Toast.LENGTH_LONG).show();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {

                ProgressBar mProgress=dialog.findViewById(R.id.upload_progress);
                load = dialog.findViewById(R.id.loading_progress);
                load.setVisibility(View.VISIBLE);
                dialog.show();


            }
        });



    }



}