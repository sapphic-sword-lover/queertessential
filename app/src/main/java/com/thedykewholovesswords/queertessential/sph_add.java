package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.thedykewholovesswords.queertessential.model.sph_model;

public class sph_add extends AppCompatActivity {

    EditText mName,mPh;
    Button mSubmit;
    FirebaseFirestore fStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sph_add);
        getSupportActionBar().setTitle("Add Suicide Prevention Hotline");

        mName=findViewById(R.id.sph_add_name);
        mPh=findViewById(R.id.sph_add_ph);


        mSubmit=findViewById(R.id.sph_add_submit);

        fStore=FirebaseFirestore.getInstance();


        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name=mName.getText().toString().trim();
                String ph=mPh.getText().toString().trim();
                onBackPressed();






                if(TextUtils.isEmpty(Name))
                {
                    mName.setError("Field Mandatory");
                    return;
                }

                if(TextUtils.isEmpty(ph))
                {
                    mPh.setError("Field Mandatory");
                    return;
                }
                else if(ph.length()<9 || ph.length()>11)
                {
                    mPh.setError("Phone Number must be 10 digit or 11 Digit");
                    return;


                }



                sph_model dm=new sph_model(Name,ph);

                DocumentReference documentReference=fStore.collection("sph").document();

                documentReference.set(dm).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "Entry Added Successfully", Toast.LENGTH_SHORT).show();





                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("add123",""+e);
                    }
                });



            }
        });
    }
}