package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.thedykewholovesswords.queertessential.RecycleViewAdapters.CommentsRvAdapter;
import com.thedykewholovesswords.queertessential.UserInformation.UserRetriver;
import com.thedykewholovesswords.queertessential.UserInformation.User_model;
import com.thedykewholovesswords.queertessential.model.comments_model;

import java.security.Key;

public class MemesComments extends AppCompatActivity {

    String PostID;
    TextView test;
    UserRetriver UR;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    TextView mDName;
    ImageButton mCommentPost;
    CommentsRvAdapter adapter;
    EditText mCommentET;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memes_comments);
        getSupportActionBar().setTitle("Comments");

        fAuth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();

        Bundle bundle=getIntent().getExtras();
        PostID=bundle.getString("PostID");

        mDName=findViewById(R.id.memes_comments_displayname);
        mCommentPost=findViewById(R.id.memes_comment_send_btn);
        mCommentET=findViewById(R.id.memes_comment_ET);

        UR=new UserRetriver(fAuth.getCurrentUser().getUid());
        UR.readData(new UserRetriver.FirebaseCallBack() {
            @Override
            public void onCallback(User_model userModel) {
                mDName.setText(userModel.getdName());

            }
        });

        mCommentPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MemesComments.this, ""+mDName.getText().toString(), Toast.LENGTH_SHORT).show();

                UploadComment(mCommentET.getText().toString());
                mCommentET.setText("");
            }
        });

        setUpCommentRecyclerView();
    }

    private void UploadComment(String Comm) {
        String Comment=Comm;
        String UUID=fAuth.getCurrentUser().getUid();
        String DisplayName=mDName.getText().toString();

        if(TextUtils.isEmpty(Comment))
        {
            return;
        }
        comments_model CMM=new comments_model();
        CMM.setName(DisplayName);
        CMM.setComment(Comment);
        CMM.setUID(UUID);

        DocumentReference documentReference=fStore.collection("memes").document(PostID).collection("comments").document();
        documentReference.set(CMM).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                setUpCommentRecyclerView();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MemesComments.this, "Error! "+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });



    }


    private void setUpCommentRecyclerView() {
        Query query=fStore.collection("memes").document(PostID).collection("comments").orderBy("timestamp");
        FirestoreRecyclerOptions<comments_model> options=new FirestoreRecyclerOptions.Builder<comments_model>().setQuery(query,comments_model.class).build();
        adapter=new CommentsRvAdapter(options);
        RecyclerView recyclerView=findViewById(R.id.memes_comments_rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm=new LinearLayoutManager(this);
        lm.setStackFromEnd(true);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onStart() {
        super.onStart();
    adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}