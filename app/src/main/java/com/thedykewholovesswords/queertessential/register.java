package com.thedykewholovesswords.queertessential;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

public class register extends AppCompatActivity {

    EditText mDisplay, mEmail,mPassword;
    Button mSignUp;
    ProgressBar mProgressbar;
    FirebaseAuth fauth;
    String userid;
    FirebaseFirestore fStore;
    LottieAnimationView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mDisplay=findViewById(R.id.register_disp_name);
        mEmail=findViewById(R.id.register_email);
        mPassword=findViewById(R.id.register_password);
        mProgressbar=findViewById(R.id.register_progressbar);
        loading = findViewById(R.id.loading_amim);

        mSignUp=findViewById(R.id.register_signup);

        fauth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();



        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String display=mDisplay.getText().toString().trim();
                String email=mEmail.getText().toString().trim();
                String password=mPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Email is Mandatory");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    mPassword.setError("Password is Mandatory");
                    return;
                }
                if(TextUtils.isEmpty(display)){
                    mDisplay.setError("Display Name is Mandatory");
                    return;
                }
                if(password.length()<6)
                {
                            mPassword.setError("Password must be greater than 6 characters");
                            return;
                }
                mProgressbar.setVisibility(View.INVISIBLE);
                loading.setVisibility(View.VISIBLE);
                fauth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(register.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                            Log.d("abc123","Successfully registered!");


                            userid=fauth.getCurrentUser().getUid();
                           /* Log.d("abc123",""+userid);

                            DocumentReference documentReference=fStore.collection("users").document(userid);

                            Map<String,Object>  usermap=new HashMap<>();
                            usermap.put("dName",display);
                            usermap.put("email",email);

                            documentReference.set(usermap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("abc123","Successfully written!");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("abc123",""+e);
                                }
                            });

                            */

                            Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            adddisplayname(userid,display,email);


                        }
                        else
                        {
                            Toast.makeText(register.this, "Error!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            mProgressbar.setVisibility(View.INVISIBLE);
                            loading.setVisibility(View.INVISIBLE);
                        }
                    }
                });



            }
        });




    }
    public void adddisplayname(String uid, String disp_name,String email)
    {
        DocumentReference documentReference=fStore.collection("users").document(uid);

        Map<String,Object>  usermap=new HashMap<>();
        usermap.put("dName",disp_name);
        usermap.put("email",email);

        documentReference.set(usermap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("abc123","Successfully written!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("abc123",""+e);
            }
        });

    }


    public void open_login(View view) {
        startActivity(new Intent(getApplicationContext(),login.class));
        finish();
    }
}